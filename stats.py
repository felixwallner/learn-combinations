#!/usr/bin/python3
"""Provides statistical analysis for active automata learned experiments"""

__author__ = "Felix Wallner, Martin Tappler"
__version__ = "1.2.0"

import argparse
import csv
import filecmp
import itertools
import os
import re
import sys
from collections import Counter
from pathlib import Path

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

try:
    from tikzplotlib import save as tikz_save
except ImportError:
    tikz_save = None
    print("WARN: tikzplotlib is not installed, no tikz plots will be created")


class Experiment(object):
    """This class represents an experiment and all data about it.
    Initialized with path, statistical analysis will take place on init.
    path: str - Path to csv file of this experiment
    filename: str - Pure filename (without path)
    la: str - Learning algorithm of experiment (extracted from 'filename')
    ta: str - Testing algorithm of experiment (extracted from 'filename')
    data: dict - Pure data extracted from csv file at 'path'
    stats: dict - Dict of categories each holding a dict with statistical values from that category
    classes: list - List of all classes (from classification file) this experiment belongs to
    """

    def __init__(self, path):
        self.path = path
        self.filename, self.la, self.ta = self.learn_config_from_file_name(self.path)
        self.combination = f"{self.la}_{self.ta}"
        self.data = self.readCsvData(self.path)
        self.stats = {k: self.compute_statistics(v) for k, v in self.data.items()}
        self.classes = []  # may be filled later

    def __repr__(self):
        return f"{self.filename}_{self.combination}"

    def getStat(self, category, stat):
        return self.stats[category][stat]

    @staticmethod
    def compute_statistics(data_list):
        return {
            "len": len(data_list),
            "min": np.min(data_list),
            "max": np.max(data_list),
            "mean": np.mean(data_list),
            "1st-quart": np.percentile(data_list, 25),
            "median": np.percentile(data_list, 50),
            "3rd-quart": np.percentile(data_list, 75),
            "std": np.std(data_list),
        }

    @staticmethod
    def learn_config_from_file_name(file_name):
        m = re.search("([A-Za-z0-9 _.\\-]+)_([A-Z]+)_([A-Z]+)\\.csv", file_name)
        return (m.group(1), m.group(2), m.group(3))

    @staticmethod
    def readCsvData(file_name):
        with open(file_name) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=",")
            data = dict()
            for row in csv_reader:
                if row:
                    if len(row) == 1:
                        print(f"WARN: Single row {row[0]} in file {file_name}")
                        continue
                    type_of_data = row[0]
                    if "Seed" in type_of_data or "Correct" in type_of_data:
                        continue
                    raw_data = row[1:]
                    data[type_of_data] = list(map(int, raw_data))
        return data


# end class Experiment


def find_all_files_with_suffix(path, suffix, depth=None):
    """Returns a set of relative paths of files ending with suffix.
    If depth is an int, only go <depth> directly levels deeper"""
    files = set()
    for r, d, f in os.walk(path):
        if depth is not None and r.count(os.sep) > depth:
            print(f"INFO: Ignoring files in depth {r.count(os.sep)}:", r)
            continue
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def collect_experiment_filesnames(path, inclusive, exclusive, dirdepth=None):
    """Return a list of all paths of csv files in path that include 'inclusive' and exclude 'exclusive'.
    Exit the program if this results in no files."""
    experiments = find_all_files_with_suffix(path, ".csv", dirdepth)
    if not experiments:
        exit_err(f"No csv files were found in location {path}")

    if inclusive:
        experiments = [exp for exp in experiments if all(i in exp for i in inclusive)]
        if not experiments:
            exit_err(f"No csv files remain after filtering for {inclusive}")

    if exclusive:
        experiments = [exp for exp in experiments if all(e not in exp for e in exclusive)]
        if not experiments:
            exit_err(f"No csv files remain after excluding {exclusive}")

    print(f"INFO: Collected {len(experiments)} files for further analysis")
    name_counter = Counter([Path(exp).name for exp in experiments])
    if any(val > 1 for val in name_counter.values()):
        print(f"WARN: Found duplicated experiments...")
        for name, val in name_counter.most_common():
            if val <= 1:
                break
            print(f"WARN:    {name} appears {val} times")
            compare = [Path(exp) for exp in experiments if Path(exp).name == name]
            for a, b in zip(compare, compare[1:]):
                if not filecmp.cmp(a, b, shallow=False):
                    print("ERROR: ", a, b, " are NOT identical!")
                # else:
                #     if len(a.as_posix()) > len(b.as_posix()):
                #         print("INFO: deleting", a)
                #         a.unlink()
                #     else:
                #         print("INFO: deleting", b)
                #         b.unlink()
    return experiments


def add_experiment_classification(exps, classificationpath):
    """Reads file at path 'classificationpath' and enriches all exps with given classes there."""
    try:
        with open(classificationpath, "r") as f:
            classification = {
                k.strip(): v.strip() for (k, v) in (line.split(":") for line in f.readlines())
            }
    except FileNotFoundError as e:
        exit_err(f'Classification file "{classificationpath}" could not be found')

    not_classified = []
    for exp in exps:
        try:
            exp.classes = [
                classifier.strip() for classifier in classification[exp.filename].split(",")
            ]
        except KeyError as e:
            not_classified.append(exp.filename)

    if not_classified:
        print(
            f"WARN: {len(not_classified)} experiments were not classified in {classificationpath}: {not_classified}"
        )


def filter_experiments_by_class(exps, classification, disjunctive=False):
    """Return a new list of exps where every exp has custom classifications, either all of them conjunction=True or any of them otherwise."""
    if not disjunctive:
        result = [exp for exp in exps if all(cl in exp.classes for cl in classification)]
        classes_string = '" and "'.join(classification)
    else:
        result = [exp for exp in exps if any(cl in exp.classes for cl in classification)]
        classes_string = '" or "'.join(classification)
    msg = f'After filtering for custom class{"es" if len(classification) > 1 else ""} "{classes_string}", {len(result)} from {len(exps)} experiments remain.'

    if result:
        print(f"INFO: {msg}")
    else:
        exit_err(msg)
    if len(result) == len(exps):
        print(f"WARN: Your class filter seems to have no effect on the chosen experiments.")
    return result


def output_stat_by_experiment(experiments, category, stat):
    """For all experiments output the chosen stat from the chosen category,
    in ascending order and by alphabetically ordered experiment.
    """
    print(f"OUTPUT: '{stat}' of '{category}' by experiment:")
    last_name = None
    for e in sorted(experiments, key=lambda e: (e.filename.lower(), e.getStat(category, stat))):
        if last_name != e.filename:
            last_name = e.filename
            print(f'{"="*50}\nExperiment:{" "*4}{e.filename}\n{"="*50}')
        print(f'({e.la:7}, {e.ta:18}):{" "*6}{e.getStat(category, stat):.1f}')
    print(f'{"="*50}\n')


def output_sum_of_average_stat_by_combination(
    experiments, category, stat, normalize=False, score_missing=None
):
    """For all experiments output the chosen stat from the chosen category,
    in ascending order and by alphabetically ordered experiment.
    """
    print(
        f"OUTPUT: {'Normalized ' if normalize else ''}sum of stat: '{stat}' of '{category}' by la-ta combination{(' with scoring missing ' + str(score_missing)) if score_missing is not None else ''}:"
    )

    # sort by experiment (to get norm)
    max_stats = {}
    if normalize:
        max_stats = {
            k: max((exp.getStat(category, stat) for exp in v))
            for k, v in itertools.groupby(
                sorted(experiments, key=lambda e: e.filename), key=lambda e: e.filename
            )
        }

    result = []
    # sorting and splitting by combination
    sorting_key = lambda e: f"{e.la}_{e.ta}"

    if score_missing is not None:
        longest = -1
        for k, v in itertools.groupby(sorted(experiments, key=sorting_key), key=sorting_key):
            longest = max(longest, len(list(v)))
        # print(f"INFO: Most experiments per combinations are {longest}")

    for k, v in itertools.groupby(sorted(experiments, key=sorting_key), key=sorting_key):
        comb = list(v)
        penalty = 0
        if score_missing is not None:
            missing = longest - len(comb)
            if missing > 0:
                penalty = missing * score_missing
        result.append(
            (
                k,
                sum(s.getStat(category, stat) / max_stats.get(s.filename, 1.0) for s in comb)
                + penalty,
            )
        )

    for t in sorted(result, key=lambda x: x[1]):
        print(f'({t[0]:26}):{" "*6}{t[1]:.4f}')
    print(f'{"="*50}\n')


def output_average_stat_of_combination(
    experiments, category, stat, normalize=False, score_missing=None
):
    """For all experiments output the average (div by num experiments of comb)
    chosen stat from the chosen category,
    in ascending order and by alphabetically ordered experiment.
    """
    print(
        f"OUTPUT: {'Normalized ' if normalize else ''}average of stat: '{stat}' of '{category}' by la-ta combination{(' with scoring missing ' + str(score_missing)) if score_missing is not None else ''}:"
    )

    # sort by experiment (to get norm)
    max_stats = {}
    if normalize:
        max_stats = {
            k: max((exp.getStat(category, stat) for exp in v))
            for k, v in itertools.groupby(
                sorted(experiments, key=lambda e: e.filename), key=lambda e: e.filename
            )
        }

    # sort by comb
    sorting_key = lambda e: f"{e.la}_{e.ta}"
    combs = {}
    for exp in experiments:
        key = sorting_key(exp)
        combs[key] = combs.get(key, []) + [exp]

    if score_missing is not None:
        longest = max(len(val) for val in combs.values())
        print(f"INFO: Most experiments per combinations are {longest}")

    result = []
    for comb, comb_exps in combs.items():
        # stats = [exp.getStat(category, stat) for exp in comb_exps]
        stats = [
            exp.getStat(category, stat) / max_stats.get(exp.filename, 1.0) for exp in comb_exps
        ]
        if score_missing is not None:
            while len(stats) < longest:
                stats.append(score_missing)
        avg_stat = np.mean(stats)
        result.append((comb, avg_stat, comb_exps))

    for t in sorted(result, key=lambda x: x[1]):
        print(f'({t[0]:26}):{" "*2}{t[1]:12.2}{" "*2}({len(t[2])})')
    print(f'{"="*50}\n')


def output_ranking_of_sum_of_stat_by_combination(experiments, category, stat):
    """For all experiments sum up the ranking of each sum of stat by combination."""
    print(f"OUTPUT: Ranking of sum of stat: '{stat}' of '{category}' by experiment:")
    result = {}

    last_name = None
    for e in sorted(experiments, key=lambda e: (e.filename.lower(), e.getStat(category, stat))):
        if last_name != e.filename:
            rank = 1
            last_name = e.filename
        result[(e.la, e.ta)] = result.get((e.la, e.ta), 0) + rank
        rank += 1

    for k, v in sorted(result.items(), key=lambda x: x[1]):
        print(f'({f"{k[0]}_{k[1]}":26}):{" "*6}{v:.0f}')
    print(f'{"="*50}\n')


def plot_num_exp_vs_category_stat(
    experiments, category, stat, partialComb=None, logy=True, savedir=None
):
    sorting_key = lambda e: e.combination
    # markers = ["x", "|", "^", "*", "."]
    # colors = [
    #     "#1f77b4",
    #     "#ff7f0e",
    #     "#2ca02c",
    #     "#d62728",
    #     "#9467bd",
    #     "#8c564b",
    #     "#e377c2",
    #     "#7f7f7f",
    #     "#bcbd22",
    #     "#17becf",
    # ]
    las = sorted(set([e.la for e in experiments]))
    las_ord = ["LSTAR", "LSTARRS", "KV", "TTT", "ADT"]
    las = [la for la in las_ord + sorted(set(las).difference(las_ord)) if la in las]
    tas = sorted(set([e.ta for e in experiments]))
    # color = {ta: c for ta, c in zip(tas, colors)}
    # marker = {la: m for la, m in zip(las, markers)}
    for k, v in itertools.groupby(sorted(experiments, key=sorting_key), key=sorting_key):
        comb = list(v)
        la = comb[0].la  # sorted by la - ta combination (same for all in comb)
        ta = comb[0].ta  # sorted by la - ta combination (same for all in comb)
        if partialComb is None or (partialComb == la or partialComb == ta):
            plt.plot(
                [
                    c.getStat(category, stat)
                    for c in sorted(comb, key=lambda e: e.getStat(category, stat))
                ],
                "-*",
                # "-" + marker.get(la, "o"),
                fillstyle="none",
                label=f"{la}_{ta}",
                # color=color.get(ta, "red"),
            )

    plt.title(
        f"#Experiments with at most Y {stat} of {category} for all {'' if partialComb is None else partialComb + ' '}experiments"
    )
    plt.xlabel("Number of Experiments")
    plt.ylabel(f"{stat} of {category}")
    plt.legend()
    filename = f"numexp_vs_{stat}_{category}{'_log' if logy else ''}{'' if partialComb is None else f'_{partialComb}'}.svg".replace(
        " ", ""
    )
    if logy:
        plt.yscale("log")
        # plt.ylim((10**3, 10**11))
    else:
        plt.yscale("linear")
        plt.ylim((1, 100000))

    if savedir is not None:
        fig = matplotlib.pyplot.gcf()
        if tikz_save is not None:
            savedloc = f"{savedir.strip()}/{filename}.tex"
            # TODO: required a change in tikzplotlib/_legend.py draw_legend:
            # if hasattr(obj, "_ncol"): ... else: _ncols ...
            tikz_save(savedloc, figure=fig, strict=True)
            with open(savedloc, "r") as f:
                file = f.read()
            for old, new in {
                "LSTARRS": "RS",
                "LSTAR": "L$^*$",
                "RANDOMWPMETHOD": "random Wp",
                "WMETHOD": "W-method",
                "WPMETHOD": "Wp-method",
                "RANDOMWALKS": "random walks",
                "RANDOMWORDS": "random words",
                "MUTATION": "mutation",
                "TRANSITIONCOVERAGE": "transition c.",
                "_": "-",
            }.items():
                file = file.replace(old, new)
            with open(savedloc, "w") as f:
                f.write(file)
        # fig = matplotlib.pyplot.gcf()
        fig.set_size_inches(18.5, 10.5)
        fig.savefig(f"{savedir.strip()}/{filename}")
        plt.close(fig)
    else:
        plt.show()


def plot_bar(exps, category, stat, normalize=False, score_missing=None, savedir=None):
    # sort by experiment (to get norm)
    max_stats = {}
    if normalize:
        max_stats = {
            k: max((exp.getStat(category, stat) for exp in v))
            for k, v in itertools.groupby(
                sorted(exps, key=lambda e: e.filename), key=lambda e: e.filename
            )
        }

    result = []
    # sorting and splitting by combination
    sorting_key = lambda e: f"{e.la}_{e.ta}"

    if score_missing is not None:
        longest = -1
        for k, v in itertools.groupby(sorted(exps, key=sorting_key), key=sorting_key):
            longest = max(longest, len(list(v)))

    for k, v in itertools.groupby(sorted(exps, key=sorting_key), key=sorting_key):
        comb = list(v)
        penalty = 0
        if score_missing is not None:
            missing = longest - len(comb)
            if missing > 0:
                penalty = missing * score_missing
        result.append(
            (
                k,
                sum(s.getStat(category, stat) / max_stats.get(s.filename, 1.0) for s in comb)
                + penalty,
            )
        )

    las = sorted(set([e.la for e in exps]))
    las_ord = ["LSTAR", "LSTARRS", "KV", "TTT", "ADT"]
    las = [la for la in las_ord + sorted(set(las).difference(las_ord)) if la in las]
    tas = sorted(set([e.ta for e in exps]))

    labels = tas

    x = np.arange(len(labels))  # the label locations
    width = 0.15  # the width of the bars

    fig, ax = plt.subplots()

    for i, la in enumerate(las):
        hights = [next(res[1] for res in result if res[0] == f"{la}_{ta}") for ta in tas]
        rect = ax.bar(x + (i - len(las) / 2 + 0.5) * width, hights, width, label=la)
        # ax.bar_label(rect, padding=3)

    # Add some text for labels, title and custom x-axis tick labels, etc.
    if normalize:
        ax.set_ylabel("s2")
    else:
        ax.set_ylabel("s1")
    ax.set_xticks(x, labels)
    if not normalize:
        ax.set_yscale("log")
    ax.legend()

    fig.tight_layout()
    filename = f"{'norm_' if normalize else ''}sum_{stat}_{category}_by_comb"
    if score_missing is not None:
        filename += f"_score_missing_{score_missing}"

    if tikz_save is not None:
        savedloc = f"{savedir.strip()}/{filename}.tex"
        # TODO: required a change in tikzplotlib/_legend.py draw_legend:
        # if hasattr(obj, "_ncol"): ... else: _ncols ...
        tikz_save(savedloc, figure=fig, strict=True)
        with open(savedloc, "r") as f:
            file = f.read()
        for old, new in {
            "LSTARRS": "RS",
            "LSTAR": "L$^*$",
            "RANDOMWPMETHOD": "random Wp",
            "WMETHOD": "W-method",
            "WPMETHOD": "Wp-method",
            "RANDOMWALKS": "random walks",
            "RANDOMWORDS": "random words",
            "MUTATION": "mutation",
            "TRANSITIONCOVERAGE": "transition c.",
        }.items():
            file = file.replace(old, new)
        with open(savedloc, "w") as f:
            f.write(file)
    if savedir is not None:
        fig = matplotlib.pyplot.gcf()
        fig.set_size_inches(18.5, 10.5)
        fig.savefig(f"{savedir.strip()}/{filename}.svg")
        plt.close(fig)
    else:
        plt.show()


def plot_exps_by_classes(experiments, category, stat, logy=True, savedir=None):
    all_classes = set()
    for e in experiments:
        for c in e.classes:
            all_classes.add(c)
    class_split = []
    for c in all_classes:
        class_split.append([e for e in experiments if c in e.classes])

    x = np.arange(len(all_classes))  # the label locations
    width = 0.35  # the width of the bars

    means = [np.mean([e.getStat(category, stat) for e in aclass]) for aclass in class_split]
    medians = [
        np.percentile([e.getStat(category, stat) for e in aclass], 50) for aclass in class_split
    ]

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width / 2, means, width, label="Mean")
    rects2 = ax.bar(x + width / 2, medians, width, label="Median")

    if logy:
        ax.set_yscale("log")

    ax.set_xticks(x)
    ax.set_xticklabels(all_classes)
    ax.legend()
    fig.tight_layout()
    fig.savefig(f"{savedir.strip()}/all_classes_plot{'_log' if logy else ''}.svg")
    plt.close(fig)


def parseargs():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--version", action="version", version=f"Version: {__version__}; Authors: {__author__}"
    )
    parser.add_argument(
        "-p",
        "--path",
        help="Directory path to read csv files from. Defaults to 'out'.",
        default="out",
        type=str,
    )
    parser.add_argument(
        "-o",
        "--outdir",
        help="Directory where to save plots and other results. Defaults to 'out_stats'.",
        default="out_stats",
        type=str,
    )
    parser.add_argument(
        "--logy", help="FLAG: Set y scale to logarithmic in plots.", action="store_true"
    )
    parser.add_argument(
        "--union",
        help="FLAG: Only evaluate experiments that exist for ALL combinations.",
        action="store_true",
    )
    parser.add_argument(
        "-cp",
        "--class-path",
        help="Path to classification file in which experiments may be assigned to arbitrary classes.",
        default="classification.txt",
        type=str,
    )
    parser.add_argument(
        "-cd",
        "--classes-disjunctive",
        help="FLAG: Use set classes (-classes) disjunctive instead of conjunctive.",
        action="store_true",
    )
    parser.add_argument(
        "-cl",
        "--class",
        help="Only show experiments of given class. Custom classification must be provided with 'class-path'.",
        action="append",
        default=[],
        type=str,
        dest="cl",
    )
    parser.add_argument(
        "-c",
        "--category",
        help="Category of which to calculate statistical values. Defaults to 'ALL Steps'.",
        default="ALL Steps",
        type=str,
    )
    parser.add_argument(
        "-s",
        "--stat",
        help="Statistical value to calculate. Defaults to 'mean'.",
        default="mean",
        type=str,
    )
    parser.add_argument(
        "-i",
        "--inclusive",
        help="Only use files containing certain strings. Must contain all given strings.",
        action="append",
        default=[],
        type=str,
    )
    parser.add_argument(
        "-e",
        "--exclusive",
        help="Only use files NOT containing certain strings. Must not contain any given string.",
        action="append",
        default=[],
        type=str,
    )
    parser.add_argument(
        "-dd",
        "--dirdepth",
        help="Directoy depth in which to search for input files.",
        default=None,
        type=int,
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-os",
        "--only-stats",
        action="store_true",
        help="FLAG: Only output chosen stat by experiment",
    )
    group.add_argument(
        "-oa",
        "--only-aggregated",
        action="store_true",
        help="FLAG: Only output aggregated sums and rank",
    )
    group.add_argument(
        "-op", "--only-plots", action="store_true", help="FLAG: Only output chosen plots"
    )
    return parser.parse_args()


def exit_err(msg):
    print(f"ERR: {msg}")
    exit(-1)


def should_output(t, args):
    if args.only_aggregated:
        if t == "aggregated":
            return True
    elif args.only_stats:
        if t == "stats":
            return True
    elif args.only_plots:
        if t == "plots":
            return True
    else:
        return True
    return False


def find_missing_combinations(exps, input_models_path=None, union=False):
    """Find all missing combinations of learn- and test algorithms.
    If input_models_path is given, the input models (.dot files) with the same names
    will be searched through and will be copied into the folder 'missing' for processing."""
    from shutil import copyfile, rmtree

    las = sorted(set([e.la for e in exps]))
    tas = sorted(set([e.ta for e in exps]))
    names = sorted(set([e.filename for e in exps]))

    if input_models_path:
        automata: list[Path] = []
        for r, d, f in os.walk(input_models_path):
            for file in f:
                if file.endswith(".dot"):
                    automata.append(Path(os.path.join(r, file)))
        print(f"INFO: {len(automata)} automata found")
        assert len(automata) == len(names), f"{len(automata)} != {len(names)}"

    print(f"INFO: Experiments composed of {len(names)} different automata (benchmarking models)")
    print(f"INFO: There are {len(las)} learning algorithms: {las}")
    print(f"INFO: There are {len(tas)} testing algorithms: {tas}")
    ecombs = [(e.filename, e.la, e.ta) for e in exps]
    if input_models_path:
        missing_path = Path("./missing")
        if missing_path.exists():
            rmtree(missing_path)
        missing_path.mkdir(exist_ok=True)

    missing_comb_names = set()
    for la, ta in itertools.product(las, tas):
        missing = [name for name in names if (name, la, ta) not in ecombs]
        if missing:
            print(f"INFO: {la}_{ta} is missing: {len(missing)}")  # : {missing}")
            missing_comb_names.update(missing)
            if input_models_path:
                path = missing_path / f"{la}_{ta}"
                path.mkdir()
                for name in missing:
                    automaton = next((a for a in automata if a.stem == name))
                    copyfile(automaton, path / automaton.name)
        # else:
        #     print(f"Comb: {la}_{ta} is complete")
    if union:
        new_exps = [exp for exp in exps if exp.filename not in missing_comb_names]
        if len(exps) == len(new_exps):
            return exps
        print(f"INFO: Removed {len(exps) - len(new_exps)} experiements due to union argument")
        las = sorted(set([e.la for e in new_exps]))
        tas = sorted(set([e.ta for e in new_exps]))
        names = sorted(set([e.filename for e in new_exps]))
        print(
            f"INFO: {len(new_exps)} experiements ({len(las)} las, {len(tas)} tas and {len(names)} different automata) remain"
        )
        if len(new_exps) == 0:
            exit_err("No experiments remain after performing union")
        return new_exps

    return exps


def main():
    args = parseargs()
    print("CMND:", " ".join(sys.argv))
    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)
    paths = collect_experiment_filesnames(args.path, args.inclusive, args.exclusive, args.dirdepth)
    exps = [Experiment(p) for p in paths]
    exps = find_missing_combinations(exps, union=args.union)
    # TODO: if missing .dot files should be separated for easier testing:
    # exps = find_missing_combinations(exps, "input/mealy/principle", union=args.union)

    if args.class_path:
        add_experiment_classification(exps, args.class_path)
        if should_output("plots", args):
            plot_exps_by_classes(exps, args.category, args.stat, savedir=args.outdir)

    if args.cl and args.class_path:
        exps = filter_experiments_by_class(exps, args.cl, args.classes_disjunctive)

    if should_output("stats", args):
        output_stat_by_experiment(exps, args.category, args.stat)

    if should_output("aggregated", args):
        output_sum_of_average_stat_by_combination(exps, args.category, args.stat)
        output_sum_of_average_stat_by_combination(exps, args.category, args.stat, normalize=True)
        output_ranking_of_sum_of_stat_by_combination(exps, args.category, args.stat)
        output_average_stat_of_combination(exps, args.category, args.stat, normalize=False)
        output_average_stat_of_combination(exps, args.category, args.stat, normalize=True)
        if not args.union:
            output_sum_of_average_stat_by_combination(
                exps, args.category, args.stat, normalize=True, score_missing=1
            )
            # output_average_stat_of_combination(
            #     exps, args.category, args.stat, normalize=True, score_missing=1
            # )

    if should_output("plots", args):
        for comb in set(e.la for e in exps).union(set(e.ta for e in exps)):
            plot_num_exp_vs_category_stat(
                exps,
                args.category,
                args.stat,
                partialComb=comb,
                logy=args.logy,
                savedir=args.outdir,
            )
        plot_num_exp_vs_category_stat(
            exps, args.category, args.stat, logy=args.logy, savedir=args.outdir
        )  # this one plots ALL exps at once
        plot_bar(exps, args.category, args.stat, normalize=False, savedir=args.outdir)
        plot_bar(exps, args.category, args.stat, normalize=True, savedir=args.outdir)
        plot_bar(
            exps, args.category, args.stat, normalize=True, savedir=args.outdir, score_missing=1
        )


if __name__ == "__main__":
    main()
