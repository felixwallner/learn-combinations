package choices;

public enum LearningAlgorithmChoice {
    LSTAR,
    LSTAR_RS,
    TTT,
    KV,
    RPNI,
    ADT
}
