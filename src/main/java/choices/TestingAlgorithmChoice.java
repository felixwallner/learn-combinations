package choices;

public enum TestingAlgorithmChoice {
    W_METHOD(true),
    WP_METHOD(true),
    RANDOM_WP_METHOD(false),
    RANDOM_WALKS(false),
    RANDOM_WORDS(false),
    MUTATION(false),
    TRANSITION_COVERAGE(false);

    private Boolean deterministic = false;

    public Boolean isDeterministic() {
        return this.deterministic;
    }

    private TestingAlgorithmChoice(Boolean deterministic) {
        this.deterministic = deterministic;
    }
}
