package choices;

import java.io.Serializable;

public class TestingAlgorithmVariables implements Serializable {
    private long maxDepth;
    private long maxTests;
    private long seed = 42;
    private long randomWPMinimalLength = 0;
    private long randomWPExpectedLength = 4;
    private double randomWalkRestartProbability = 1.0 / 30.0;
    private long randomWordsMinimalLength = 10;
    private long randomWordsMaximumLength = 50;
    private long mutationSplitStateDepth = 1;
    private long mutationSplitStateAccSeqBound = 5; // 100 requires too much memory
    private boolean mutationSplitStateAllowDifLastAcc = true;
    private boolean mutationSplitStateAllowEqualPreState = true;
    private boolean mutationTestSelectorKillAliveMutants = false;
    private double mutationTraceGenStopProb = 1.0 / 30.0;
    private double mutationTraceGenRetryProb = 1 - 1.0 / 30.0;
    private long mutationTraceGenMaxTestLength = 50;
    private long mutationTraceGenMaxRandWordLen = 4;
    private long mutationTestSelectionSuiteSizeFactor = 100;
    private boolean mutationTestSelectionReuseRemaining = false;
    private boolean transitionCoverageTestSelectorKillAliveMutants = false;
    private double transitionCoverageTraceGenStopProb = 1.0 / 30.0;
    private double transitionCoverageTraceGenRetryProb = 1 - 1.0 / 30.0;
    private long transitionCoverageTraceGenMaxTestLength = 50;
    private long transitionCoverageTraceGenMaxRandWordLen = 4;
    private long transitionCoverageTestSelectionSuiteSizeFactor = 100;
    private boolean transitionCoverageTestSelectionReuseRemaining = false;

    public TestingAlgorithmVariables(long maxDepth, long maxTests) {
        this.maxDepth = maxDepth;
        this.maxTests = maxTests;
    }

    public TestingAlgorithmVariables(TestingAlgorithmVariables v) {
        maxDepth = v.maxDepth;
        maxTests = v.maxTests;
        seed = v.seed;
        randomWPMinimalLength = v.randomWPMinimalLength;
        randomWPExpectedLength = v.randomWPExpectedLength;
        randomWalkRestartProbability = v.randomWalkRestartProbability;
        randomWordsMinimalLength = v.randomWordsMinimalLength;
        randomWordsMaximumLength = v.randomWordsMaximumLength;
        mutationSplitStateDepth = v.mutationSplitStateDepth;
        mutationSplitStateAccSeqBound = v.mutationSplitStateAccSeqBound;
        mutationSplitStateAllowDifLastAcc = v.mutationSplitStateAllowDifLastAcc;
        mutationSplitStateAllowEqualPreState = v.mutationSplitStateAllowEqualPreState;
        mutationTestSelectorKillAliveMutants = v.mutationTestSelectorKillAliveMutants;
        mutationTraceGenStopProb = v.mutationTraceGenStopProb;
        mutationTraceGenRetryProb = v.mutationTraceGenRetryProb;
        mutationTraceGenMaxTestLength = v.mutationTraceGenMaxTestLength;
        mutationTraceGenMaxRandWordLen = v.mutationTraceGenMaxRandWordLen;
        mutationTestSelectionSuiteSizeFactor = v.mutationTestSelectionSuiteSizeFactor;
        mutationTestSelectionReuseRemaining = v.mutationTestSelectionReuseRemaining;
        transitionCoverageTestSelectorKillAliveMutants = v.transitionCoverageTestSelectorKillAliveMutants;
        transitionCoverageTraceGenStopProb = v.transitionCoverageTraceGenStopProb;
        transitionCoverageTraceGenRetryProb = v.transitionCoverageTraceGenRetryProb;
        transitionCoverageTraceGenMaxTestLength = v.transitionCoverageTraceGenMaxTestLength;
        transitionCoverageTraceGenMaxRandWordLen = v.transitionCoverageTraceGenMaxRandWordLen;
        transitionCoverageTestSelectionSuiteSizeFactor = v.transitionCoverageTestSelectionSuiteSizeFactor;
        transitionCoverageTestSelectionReuseRemaining = v.transitionCoverageTestSelectionReuseRemaining;
    }

    public long getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(long maxDepth) {
        this.maxDepth = maxDepth;
    }

    public long getMaxTests() {
        return maxTests;
    }

    public void setMaxTests(long maxTests) {
        this.maxTests = maxTests;
    }

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public long getRandomWPMinimalLength() {
        return randomWPMinimalLength;
    }

    public void setRandomWPMinimalLength(long randomWPMinimalLength) {
        this.randomWPMinimalLength = randomWPMinimalLength;
    }

    public long getRandomWPExpectedLength() {
        return randomWPExpectedLength;
    }

    public void setRandomWPExpectedLength(long randomWPExpectedLength) {
        this.randomWPExpectedLength = randomWPExpectedLength;
    }

    public double getRandomWalkRestartProbability() {
        return randomWalkRestartProbability;
    }

    public void setRandomWalkRestartProbability(double randomWalkRestartProbability) {
        this.randomWalkRestartProbability = randomWalkRestartProbability;
    }

    public long getRandomWordsMinimalLength() {
        return randomWordsMinimalLength;
    }

    public void setRandomWordsMinimalLength(long randomWordsMinimalLength) {
        this.randomWordsMinimalLength = randomWordsMinimalLength;
    }

    public long getRandomWordsMaximumLength() {
        return randomWordsMaximumLength;
    }

    public void setRandomWordsMaximumLength(long randomWordsMaximumLength) {
        this.randomWordsMaximumLength = randomWordsMaximumLength;
    }

    public long getMutationSplitStateDepth() {
        return mutationSplitStateDepth;
    }

    public void setMutationSplitStateDepth(long mutationSplitStateDepth) {
        this.mutationSplitStateDepth = mutationSplitStateDepth;
    }

    public long getMutationSplitStateAccSeqBound() {
        return mutationSplitStateAccSeqBound;
    }

    public void setMutationSplitStateAccSeqBound(long mutationSplitStateAccSeqBound) {
        this.mutationSplitStateAccSeqBound = mutationSplitStateAccSeqBound;
    }

    public boolean isMutationSplitStateAllowDifLastAcc() {
        return mutationSplitStateAllowDifLastAcc;
    }

    public void setMutationSplitStateAllowDifLastAcc(boolean mutationSplitStateAllowDifLastAcc) {
        this.mutationSplitStateAllowDifLastAcc = mutationSplitStateAllowDifLastAcc;
    }

    public boolean isMutationSplitStateAllowEqualPreState() {
        return mutationSplitStateAllowEqualPreState;
    }

    public void setMutationSplitStateAllowEqualPreState(boolean mutationSplitStateAllowEqualPreState) {
        this.mutationSplitStateAllowEqualPreState = mutationSplitStateAllowEqualPreState;
    }

    public boolean isMutationTestSelectorKillAliveMutants() {
        return mutationTestSelectorKillAliveMutants;
    }

    public void setMutationTestSelectorKillAliveMutants(boolean mutationTestSelectorKillAliveMutants) {
        this.mutationTestSelectorKillAliveMutants = mutationTestSelectorKillAliveMutants;
    }

    public double getMutationTraceGenStopProb() {
        return mutationTraceGenStopProb;
    }

    public void setMutationTraceGenStopProb(double mutationTraceGenStopProb) {
        this.mutationTraceGenStopProb = mutationTraceGenStopProb;
    }

    public double getMutationTraceGenRetryProb() {
        return mutationTraceGenRetryProb;
    }

    public void setMutationTraceGenRetryProb(double mutationTraceGenRetryProb) {
        this.mutationTraceGenRetryProb = mutationTraceGenRetryProb;
    }

    public long getMutationTraceGenMaxTestLength() {
        return mutationTraceGenMaxTestLength;
    }

    public void setMutationTraceGenMaxTestLength(long mutationTraceGenMaxTestLength) {
        this.mutationTraceGenMaxTestLength = mutationTraceGenMaxTestLength;
    }

    public long getMutationTraceGenMaxRandWordLen() {
        return mutationTraceGenMaxRandWordLen;
    }

    public void setMutationTraceGenMaxRandWordLen(long mutationTraceGenMaxRandWordLen) {
        this.mutationTraceGenMaxRandWordLen = mutationTraceGenMaxRandWordLen;
    }

    public long getMutationTestSelectionSuiteSizeFactor() {
        return mutationTestSelectionSuiteSizeFactor;
    }

    public void setMutationTestSelectionSuiteSizeFactor(long mutationTestSelectionSuiteSizeFactor) {
        this.mutationTestSelectionSuiteSizeFactor = mutationTestSelectionSuiteSizeFactor;
    }

    public boolean isMutationTestSelectionReuseRemaining() {
        return mutationTestSelectionReuseRemaining;
    }

    public void setMutationTestSelectionReuseRemaining(boolean mutationTestSelectionReuseRemaining) {
        this.mutationTestSelectionReuseRemaining = mutationTestSelectionReuseRemaining;
    }

    public boolean isTransitionCoverageTestSelectorKillAliveMutants() {
        return transitionCoverageTestSelectorKillAliveMutants;
    }

    public void setTransitionCoverageTestSelectorKillAliveMutants(
            boolean transitionCoverageTestSelectorKillAliveMutants) {
        this.transitionCoverageTestSelectorKillAliveMutants = transitionCoverageTestSelectorKillAliveMutants;
    }

    public double getTransitionCoverageTraceGenStopProb() {
        return transitionCoverageTraceGenStopProb;
    }

    public void setTransitionCoverageTraceGenStopProb(double transitionCoverageTraceGenStopProb) {
        this.transitionCoverageTraceGenStopProb = transitionCoverageTraceGenStopProb;
    }

    public double getTransitionCoverageTraceGenRetryProb() {
        return transitionCoverageTraceGenRetryProb;
    }

    public void setTransitionCoverageTraceGenRetryProb(double transitionCoverageTraceGenRetryProb) {
        this.transitionCoverageTraceGenRetryProb = transitionCoverageTraceGenRetryProb;
    }

    public long getTransitionCoverageTraceGenMaxTestLength() {
        return transitionCoverageTraceGenMaxTestLength;
    }

    public void setTransitionCoverageTraceGenMaxTestLength(long transitionCoverageTraceGenMaxTestLength) {
        this.transitionCoverageTraceGenMaxTestLength = transitionCoverageTraceGenMaxTestLength;
    }

    public long getTransitionCoverageTraceGenMaxRandWordLen() {
        return transitionCoverageTraceGenMaxRandWordLen;
    }

    public void setTransitionCoverageTraceGenMaxRandWordLen(long transitionCoverageTraceGenMaxRandWordLen) {
        this.transitionCoverageTraceGenMaxRandWordLen = transitionCoverageTraceGenMaxRandWordLen;
    }

    public long getTransitionCoverageTestSelectionSuiteSizeFactor() {
        return transitionCoverageTestSelectionSuiteSizeFactor;
    }

    public void setTransitionCoverageTestSelectionSuiteSizeFactor(long transitionCoverageTestSelectionSuiteSizeFactor) {
        this.transitionCoverageTestSelectionSuiteSizeFactor = transitionCoverageTestSelectionSuiteSizeFactor;
    }

    public boolean isTransitionCoverageTestSelectionReuseRemaining() {
        return transitionCoverageTestSelectionReuseRemaining;
    }

    public void setTransitionCoverageTestSelectionReuseRemaining(
            boolean transitionCoverageTestSelectionReuseRemaining) {
        this.transitionCoverageTestSelectionReuseRemaining = transitionCoverageTestSelectionReuseRemaining;
    }
}
