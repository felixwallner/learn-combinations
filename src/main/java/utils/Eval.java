package utils;

import choices.LearningAlgorithmChoice;
import choices.TestingAlgorithmChoice;
import choices.TestingAlgorithmVariables;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Symbol;
import tracking.StatisticTrackingContainer;
import tracking.StatisticTrackingContainerFactory;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class Eval {

    public static void runAllCombinationsForLowestParameterCorrectModelAndExportCsvTypeSymbolString(
            Path inputFileName,
            Path outDir,
            MealyMachine<?, Symbol, ?, String> fm,
            Alphabet<Symbol> inputAlphabet,
            long numberOfSeeds,
            boolean runInParallel) {

        System.out.println("Running all combinations on " + inputFileName.toString() + ", which has "
                + fm.getStates().size() + " states!");

        // only use this single factory, if not running in parallel.
        // Otherwise produce new factories so the same synchronized SUL does not block
        // all the threads
        StatisticTrackingContainerFactory<Symbol, String> factory = new StatisticTrackingContainerFactory<>(fm,
                Symbol.class, String.class, inputAlphabet);

        ExecutorService executor;
        if (runInParallel) {
            int cores = Runtime.getRuntime().availableProcessors();
            System.out.println("Using " + cores + " cores...");
            executor = Executors.newFixedThreadPool(cores);
        } else {
            System.out.println("Using single core (not parallel)");
            executor = Executors.newSingleThreadExecutor();
        }

        // standard testingVars (maxDepth/Tests will be overwritten anyways)
        TestingAlgorithmVariables taVars = new TestingAlgorithmVariables(1, 1);
        List<Long> seeds;
        ParamterSearcher<Long> searcher;
        List<SingleCombinationRunner<Symbol, String>> combinations = new ArrayList<>();

        for (LearningAlgorithmChoice la : new LearningAlgorithmChoice[] {
                LearningAlgorithmChoice.LSTAR,
                LearningAlgorithmChoice.LSTAR_RS,
                LearningAlgorithmChoice.TTT,
                LearningAlgorithmChoice.KV,
                LearningAlgorithmChoice.ADT,
                // LearningAlgorithmChoice.RPNI, // run RPNI separately
        }) {
            for (TestingAlgorithmChoice ta : new TestingAlgorithmChoice[] {
                    TestingAlgorithmChoice.W_METHOD,
                    TestingAlgorithmChoice.WP_METHOD,
                    TestingAlgorithmChoice.RANDOM_WP_METHOD,
                    TestingAlgorithmChoice.RANDOM_WALKS,
                    TestingAlgorithmChoice.RANDOM_WORDS,
                    TestingAlgorithmChoice.MUTATION,
                    TestingAlgorithmChoice.TRANSITION_COVERAGE,
            }) {
                if (ta.isDeterministic()) {
                    // only 1 seeds is needed (deterministic); also use lower searcher (for depth)
                    seeds = Collections.singletonList(1L);
                    searcher = new LinearOptimalParameterSearcher(1);
                } else {
                    seeds = LongStream.range(1, numberOfSeeds + 1).boxed().collect(Collectors.toList());
                    if (ta.equals(TestingAlgorithmChoice.MUTATION)) {
                        searcher = new PercentBinaryLinearParameterSearcher(1);
                    } else if (ta.equals(TestingAlgorithmChoice.TRANSITION_COVERAGE)) {
                        searcher = new PercentBinaryLinearParameterSearcher(64);
                    } else {
                        searcher = new MultBinaryOptimalParameterSearcher(64);
                    }
                }

                if (la.equals(LearningAlgorithmChoice.RPNI)) {
                    // different settings for RPNI as too long words just don't work
                    taVars.setMutationSplitStateAccSeqBound(5);
                    taVars.setMutationTraceGenStopProb(1.0 / 10.0);
                    taVars.setMutationTraceGenRetryProb(1 - 1.0 / 10.0);
                    taVars.setMutationTraceGenMaxTestLength(20);
                    taVars.setTransitionCoverageTraceGenStopProb(1.0 / 10.0);
                    taVars.setTransitionCoverageTraceGenRetryProb(1 - 1.0 / 10.0);
                    taVars.setTransitionCoverageTraceGenMaxTestLength(20);
                    taVars.setTransitionCoverageTraceGenMaxRandWordLen(3);
                }

                if (runInParallel) {
                    // the SUL for each factory is identical (to make use of the cache).
                    // But if running in parallel, produce a new factory, so the SUL is different
                    // and the threads
                    // don't block on synchronized SUL
                    factory = new StatisticTrackingContainerFactory<>(fm, Symbol.class, String.class, inputAlphabet);
                }
                combinations.add(
                        new SingleCombinationRunner<>(inputFileName, outDir, factory, la, ta, taVars, seeds, searcher));
            }
        }

        try {
            List<Future<List<StatisticTrackingContainer<Symbol, String>>>> resList = executor.invokeAll(combinations);
            System.out.println("ALL done");
            for (Future<List<StatisticTrackingContainer<Symbol, String>>> future : resList) {
                try {
                    List<StatisticTrackingContainer<Symbol, String>> result = future.get();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executor.shutdownNow();
        }
    }
}
