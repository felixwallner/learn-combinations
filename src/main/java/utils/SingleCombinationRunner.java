package utils;

import choices.LearningAlgorithmChoice;
import choices.TestingAlgorithmChoice;
import choices.TestingAlgorithmVariables;
import tracking.StatisticTrackingContainer;
import tracking.StatisticTrackingContainerFactory;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Find the lowest parameters maxTests/maxDepth for which ALL seeds learn a
 * correct model.
 * This is equivalent to optimizing maxTests/maxDepth for each seed individually
 * until a correct model is learned
 * and then choosing the worst parameters from all results.
 * 
 * @param <I> Input type
 * @param <O> Output type
 */
public class SingleCombinationRunner<I, O> implements Callable<List<StatisticTrackingContainer<I, O>>> {
    private final Path inputFileName;
    private final Path outDir;
    private final StatisticTrackingContainerFactory<I, O> factory;
    private final LearningAlgorithmChoice la;
    private final TestingAlgorithmChoice ta;
    private final TestingAlgorithmVariables taVars;
    private final List<Long> seeds;
    private final ParamterSearcher<Long> searcher;

    /**
     * Find the lowest parameters maxTests/maxDepth for which ALL seeds learn a
     * correct model.
     * This is equivalent to optimizing maxTests/maxDepth for each seed individually
     * until a correct model is learned
     * and then choosing the worst parameters from all results.
     * 
     * @param factory  Factory already initialized with mealy that should be build
     * @param la       LearningAlgorithm to use
     * @param ta       TestingAlgorithm to use
     * @param taVars   Vars to use, except maxDepth and maxTests will be incremented
     *                 until a correct model is found
     *                 and seed will be overwritten with each value of <seeds>
     * @param seeds    List of seeds that should be used; also number of result list
     *                 length
     * @param searcher Searcher, initialized with startValue from where to start the
     *                 searching process
     * @return A list of StatisticTrackingContainers that all learned their model
     *         correctly, each with different seeds
     *         from <seeds> and all with the same values of maxTests/maxDepth
     */
    public SingleCombinationRunner(
            Path inputFileName,
            Path outDir,
            StatisticTrackingContainerFactory<I, O> factory,
            LearningAlgorithmChoice la,
            TestingAlgorithmChoice ta,
            TestingAlgorithmVariables taVars,
            List<Long> seeds,
            ParamterSearcher<Long> searcher) {
        this.inputFileName = inputFileName;
        this.outDir = outDir;
        this.factory = factory;
        this.la = la;
        this.ta = ta;
        this.taVars = taVars;
        this.seeds = seeds;
        this.searcher = searcher;
    }

    /**
     * @return A list of StatisticTrackingContainers that all learned their model
     *         correctly, each with different seeds
     *         from <seeds> and all with the same values of maxTests/maxDepth
     * @throws Exception
     */
    @Override
    public List<StatisticTrackingContainer<I, O>> call() throws Exception {
        List<StatisticTrackingContainer<I, O>> result = new ArrayList<>();
        List<TestingAlgorithmVariables> localVars = new ArrayList<>();
        seeds.forEach(s -> {
            TestingAlgorithmVariables localVar = new TestingAlgorithmVariables(taVars);
            localVar.setSeed(s);
            localVars.add(localVar);
        });
        Boolean allCorrect = null;
        Long currentListValue = null;

        Long tries = 0L;

        do {
            tries += 1;
            allCorrect = true;
            currentListValue = searcher.next();
            result.clear();
            System.out.println("Try " + tries + " of " + la.name() + ", " + ta.name() +
                    ": " + seeds.size() + " seeds with Max " + searcher.next());

            for (TestingAlgorithmVariables var : localVars) {
                var.setMaxDepth(searcher.next());
                var.setMaxTests(searcher.next());
                StatisticTrackingContainer<I, O> con = factory.getExperimentHolder(la, ta, var);
                result.add(con);
                con.getExperiment().run();
                allCorrect &= con.isLearnedModelCorrect();
                if (!allCorrect) {
                    break;
                }
            }
            searcher.partialResult(allCorrect);
        } while (!searcher.done() || !searcher.next().equals(currentListValue));
        System.out.println(
                "DONE! After " + tries + " tries with " + la.name() + ", " + ta.name() + ": " + seeds.size()
                        + " seeds with Max " + searcher.next());
        CsvUtils.exportCsvForSingleCombinationToFile(outDir, inputFileName, result);
        // System.out.println("Writing CSV DONE!");
        return result;
    }
}
