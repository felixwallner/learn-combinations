package utils;

public class PercentBinaryLinearParameterSearcher implements ParamterSearcher<Long> {
    private final long startValue;
    private final double growthFactor;
    private final double acceptPercent;
    private Long highestFailed;
    private Long lowestSuccess;
    private long current;
    private boolean lastPartialCorrect = false;

    public PercentBinaryLinearParameterSearcher() {
        this(64L);
    }

    public PercentBinaryLinearParameterSearcher(long startValue) {
        this(startValue, 2, 0.05);
    }

    public PercentBinaryLinearParameterSearcher(long startValue, double growthFactor, double acceptPercent) {
        if (startValue <= 0) {
            throw new IllegalArgumentException("StartValue must be positive.");
        } else if (growthFactor <= 1.0) {
            throw new IllegalArgumentException("Factor must be bigger than 1 for growth.");
        } else if (acceptPercent < 0.0 || acceptPercent > 1.0) {
            throw new IllegalArgumentException("acceptPercent must be between 0 and 1.");
        }
        this.startValue = startValue;
        this.growthFactor = growthFactor;
        this.acceptPercent = acceptPercent;
        reset();
    }

    @Override
    public void partialResult(Boolean condition) {
        if (condition == null) {
            throw new RuntimeException("Partial result must be either true of false, was null");
        }

//        if (done()) {
//            throw new RuntimeException("Tried to update new partial result even though searcher was already done");
//        }

        lastPartialCorrect = condition;
        if (condition) {
            lowestSuccess = current;
        } else {
            highestFailed = current;
        }
        if (!done()) {
            newCurrent();
        }
    }

    @Override
    public Long next() {
        return current;
    }

    @Override
    public boolean done() {
        if (lowestSuccess != null && lowestSuccess == 1L && current == 1L) {
            return true;
        }
        if (lowestSuccess == null || highestFailed == null) {
            return false;
        }
        double enoughPercent = acceptPercent * lowestSuccess;
        double currentPercent = (lowestSuccess - highestFailed) * lowestSuccess / 100.0;
        return (currentPercent <= enoughPercent || lowestSuccess - highestFailed <= 1) && lastPartialCorrect;
    }

    @Override
    public void reset() {
        highestFailed = null;
        lowestSuccess = null;
        current = startValue;
    }

    private void newCurrent() {
        if (highestFailed == null) {
            // too high; no lowest fail found yet
            current /= growthFactor;
        } else if (lowestSuccess == null) {
            // too low; no highest success found yet
            current *= growthFactor;
        } else if (lowestSuccess - highestFailed <= 1) {
            // binary search ran into nd, start using linear and accept next correct result
            lowestSuccess += 1;
            highestFailed += 1;
            current += 1;
        } else {
            // binary search if bounds were found
            long distance = lowestSuccess - highestFailed;
            long half = distance / 2;
            current = lowestSuccess - half;
        }

        if (current <= 0L) {
            current = 1L;
        }
    }
}
