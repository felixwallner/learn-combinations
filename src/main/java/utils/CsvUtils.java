package utils;

import choices.LearningAlgorithmChoice;
import choices.TestingAlgorithmChoice;
import tracking.StatisticTrackingContainer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CsvUtils {
    private static final char DEFAULT_SEPERATOR = ',';

    public static <I, O> void exportCsvForSingleCombinationToFile(Path outdir,
                                                                  LearningAlgorithmChoice la,
                                                                  TestingAlgorithmChoice ta,
                                                                  List<StatisticTrackingContainer<I, O>> stats) throws IOException {
        Path path = Paths.get(outdir.toString(), la.name(), ta.name(), ".csv");
        exportCsvForSingleCombinationToFile(path, stats);
    }

    public static <I, O> void exportCsvForSingleCombinationToFile(Path outDir,
                                                                  Path inputFilename,
                                                                  List<StatisticTrackingContainer<I, O>> stats) throws IOException {
        String res = generateCsvForSingleCombination(stats);
        Path outfile = FileUtils.generateFileName(inputFilename, outDir, stats.get(0).getLa(), stats.get(0).getTa(), "csv");
        FileUtils.writeToFile(outfile, res);
    }

    public static <I, O> void exportCsvForSingleCombinationToFile(Path path, List<StatisticTrackingContainer<I, O>> stats) throws IOException {
        String res = generateCsvForSingleCombination(stats);
        FileUtils.writeToFile(path, res);
    }

    public static <I, O> String generateCsvForSingleCombination(List<StatisticTrackingContainer<I, O>> stats) {
        StringBuilder csv = new StringBuilder();
        char separator = DEFAULT_SEPERATOR;

        try {
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("getSeed"), "Seed"));
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("isLearnedModelCorrect"), "Correct M"));
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("getMaxDepth"), "Max Depth"));
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("getMaxTests"), "Max Tests"));
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("getEQQueries"), "EQ Queries"));
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("getMemberQueryTests"), "MQ Tests"));
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("getEQQueryTests"), "EQ Tests"));
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("getCombinedTests"), "ALL Tests"));
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("getMemberQuerySteps"), "MQ Steps"));
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("getEQQuerySteps"), "EQ Steps"));
            csv.append(writeCsvLine(stats, separator, StatisticTrackingContainer.class.getMethod("getCombinedSteps"), "ALL Steps"));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return csv.toString();
    }

    private static <I, O> String writeCsvLine(List<StatisticTrackingContainer<I, O>> stats,
                                              char separator,
                                              Method method,
                                              String rowName) {
        StringBuilder line = new StringBuilder(rowName);
        for (StatisticTrackingContainer<I, O> con : stats) {
            line.append(separator);
            try {
                line.append(method.invoke(con));
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        line.append("\n");
        return line.toString();
    }
}
