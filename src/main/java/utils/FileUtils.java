package utils;

import choices.LearningAlgorithmChoice;
import choices.TestingAlgorithmChoice;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileUtils {

    /**
     * Will search through a directory for .dot (DOT) files and return a list with all of their paths.
     * @param directory Starting directory (upper most directory to search in)
     * @param recursive If true, will also search all sub directories
     * @return A List with Paths to all DOT files in directory
     * @throws IOException
     */
    public static List<Path> getAllDotFiles(Path directory, boolean recursive) throws IOException {
        List<Path> result = new ArrayList<>();

        if (Files.isDirectory(directory)) {
            List<Path> dirContent = Files.list(directory).collect(Collectors.toUnmodifiableList());
            for (Path f : dirContent) {
                if (Files.isRegularFile(f) && f.toString().endsWith(".dot")) {
                    result.add(f);
                } else if (recursive && Files.isDirectory(f)) {
                    result.addAll(getAllDotFiles(f, recursive));
                }
            }
        }
        return result;
    }

    public static Path removeExtension(Path path) {
        if (path == null) {
            return null;
        }

        String filename = path.toString();
        int indexExtension = filename.lastIndexOf(".");
        int indexSeparator = filename.lastIndexOf("/");
        String result = indexExtension < indexSeparator ? filename : filename.substring(0, indexExtension);
        return Paths.get(result);
    }

    public static void writeToFile(Path path, String content) throws IOException {
        byte[] result = content.getBytes(StandardCharsets.UTF_8);
        java.nio.file.Files.write(path, result, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
    }

    public static Path generateFileName(Path inputFileName, Path outDir,
                                        LearningAlgorithmChoice la, TestingAlgorithmChoice ta, String ending) {
        String name = FileUtils.removeExtension(inputFileName.getFileName()).toString();
        String laname = Arrays.stream(la.name().split("_")).map(StringUtils::capitalize).collect(Collectors.joining());
        String taname = Arrays.stream(ta.name().split("_")).map(StringUtils::capitalize).collect(Collectors.joining());
        name = name + "_" + laname + "_" + taname + "." + ending;
        return outDir.resolve(name);
    }
}
