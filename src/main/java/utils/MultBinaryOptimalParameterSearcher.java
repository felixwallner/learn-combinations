package utils;

public class MultBinaryOptimalParameterSearcher implements ParamterSearcher<Long> {
    private final long startValue;
    private final double factor;
    private Long highestFailed;
    private Long lowestSuccess;
    private long current;

    public MultBinaryOptimalParameterSearcher() {
        this(64L);
    }

    public MultBinaryOptimalParameterSearcher(long startValue) {
        this(startValue, 2);
    }

    public MultBinaryOptimalParameterSearcher(long startValue, double factor) {
        if (startValue <= 0) {
            throw new IllegalArgumentException("StartValue must be positive.");
        } else if (factor <= 1.0) {
            throw new IllegalArgumentException("Factor must be bigger than 1 for growth.");
        }
        this.startValue = startValue;
        this.factor = factor;
        reset();
    }

    @Override
    public void partialResult(Boolean condition) {
        if (condition != null && !done()) { // ignore null
            if (condition) {
                lowestSuccess = current;
            } else {
                highestFailed = current;
            }
            newCurrent();
        }
    }

    @Override
    public Long next() {
        return current;
    }

    @Override
    public boolean done() {
        if (lowestSuccess != null && lowestSuccess == 1L && current == 1L) {
            return true;
        }
        if (lowestSuccess == null || highestFailed == null) {
            return false;
        }
        return lowestSuccess - highestFailed == 1 && current == lowestSuccess;
    }

    @Override
    public void reset() {
        highestFailed = null;
        lowestSuccess = null;
        current = startValue;
    }

    private void newCurrent() {
        if (highestFailed == null) {
            // too high; no lowest fail found yet
            current /= factor;
        } else if (lowestSuccess == null) {
            // too low; no highest success found yet
            current *= factor;
        } else {
            // binary search if bounds were found
            long distance = lowestSuccess - highestFailed;
            long half = distance / 2;
            current = lowestSuccess - half;
        }

        if (current <= 0L) {
            current = 1L;
        }
    }
}
