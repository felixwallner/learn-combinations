package utils;

import choices.LearningAlgorithmChoice;
import choices.TestingAlgorithmChoice;
import tracking.StatisticTrackingAccess;
import tracking.StatisticTrackingContainerSummary;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class LatexUtils {

    public static <I, O> String buildLatexTableEntry(StatisticTrackingAccess<I, O> info) {
        String sep = " \\\\";
        StringBuilder result = new StringBuilder("\\makecell{");

        if (info.isLearnedModelCorrect()) {
            result.append("Model Correct: ");
        } else {
            result.append("Model INcorrect: ");
        }
//        result.append(la.name()).append(", ").append(ta.name()).append(sep);
        result.append(sep);

        // long version
//        result.append(memOracle.getMemQueries().toString()).append(sep);
//        result.append(memOracle.getMemQueriesDuringEQQuery().toString()).append(sep);
//        result.append(memOracle.getCombinedMemQueries().toString()).append(sep);
//        result.append(eqOracle.getEquivalentQueries().toString()).append(sep);
//        result.append("average membership queries during EQ queries: ").append(getAverageMembershipQueriesDuringEQQuery()).append(sep);

        // short version
        result.append("MQ: ").append(info.getMemberQueryTests()).append(sep);
        result.append("MQ dEQ: ").append(info.getEQQueryTests()).append(sep);
//        result.append("MQ all: ").append(info.getMemberQueryTests() +info.getEQQueryTests()).append(sep);
        result.append("MQs: ").append(info.getMemberQuerySteps()).append(sep);
        result.append("MQs dEQ: ").append(info.getEQQuerySteps()).append(sep);
//        result.append("MQs all: ").append(info.getMemberQuerySteps() + info.getEQQuerySteps()).append(sep);
        result.append("MaxD/T: ").append(info.getMaxDepth()).append(sep);
        result.append("AvgNseed: ").append(info.getNumberOfSeeds()).append(sep);
//        result.append("EQ: ").append(eqOracle.getEquivalentQueries().getCount()).append(sep);
//        result.append("AVG MQ/EQ: ").append(Math.round(getAverageMembershipQueriesDuringEQQuery())).append(sep);

        result.append("}");
        return result.toString();

    }

    public static <I, O> String buildLatexTable(StatisticTrackingContainerSummary<I, O> summary) {
        StringBuilder latex = new StringBuilder("\\begin{tabular}{");
        latex.append(" *{").append(1 + summary.getCols()).append("}{|c}| }");
        latex.append("\n\\hline\n & ");

        for (LearningAlgorithmChoice la : LearningAlgorithmChoice.values()) {
            latex.append(la.name());

            if (la.ordinal() < summary.getCols() - 1) {
                latex.append(" & ");
            } else {
                latex.append(" \\\\");
            }
        }

        for (TestingAlgorithmChoice ta : TestingAlgorithmChoice.values()) {
            if (summary.isRowEmpty(ta)) {
                continue;
            }

            latex.append("\n\\hline\n");
            latex.append(ta.name());
            latex.append(" & ");

            for (LearningAlgorithmChoice la : LearningAlgorithmChoice.values()) {
                StatisticTrackingAccess<I, O> info = summary.get(la, ta);
                latex.append(buildLatexTableEntry(info));

                if (la.ordinal() < summary.getCols() - 1) {
                    latex.append(" & ");
                } else {
                    latex.append(" \\\\");
                }
            }
        }
        latex.append("\n\\hline\n\\end{tabular}");
        return latex.toString().replaceAll("_", "-");
    }

    public static <I, O> void exportLatexTableDocument(Path filename,
              StatisticTrackingContainerSummary<I, O> summary) throws IOException {

        StringBuilder doc = new StringBuilder();
        doc.append("\\documentclass{article}\n")
                .append("\n")
                .append("\\usepackage[utf8]{inputenc}\n")
                .append("\\usepackage{fourier}\n")
                .append("\\usepackage{array}\n")
                .append("\\usepackage{makecell}\n")
                .append("\\usepackage[a4paper, margin=40px]{geometry}\n")
                .append("\n")
                .append("\\renewcommand\\theadalign{bc}\n")
                .append("\\renewcommand\\theadfont{\\bfseries}\n")
                .append("\\renewcommand\\theadgape{\\Gape[4pt]}\n")
                .append("\\renewcommand\\cellgape{\\Gape[4pt]}\n")
                .append("\n")
                .append("\\begin{document}\n")
                .append("\n");
        doc.append(buildLatexTable(summary));
        doc.append("\n")
                .append("\\end{document}");

        byte[] result = doc.toString().getBytes("utf-8");
        java.nio.file.Files.write(filename, result, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
    }
}
