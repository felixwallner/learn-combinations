package utils;

public class LinearOptimalParameterSearcher implements ParamterSearcher<Long> {
    private final long startValue;
    private final double factor;
    private long current;
    private boolean success = false;

    public LinearOptimalParameterSearcher() {
        this(1L);
    }

    public LinearOptimalParameterSearcher(long startValue) {
        this(startValue, 1);
    }

    public LinearOptimalParameterSearcher(long startValue, double factor) {
        this.startValue = startValue;
        this.factor = factor;
        reset();
    }

    @Override
    public void partialResult(Boolean condition) {
        success = condition;
        if (!done()) { // ignore null
            newCurrent();
        }
    }

    @Override
    public Long next() {
        return current;
    }

    @Override
    public boolean done() {
        return success;
    }

    @Override
    public void reset() {
        current = startValue;
        success = false;
    }

    private void newCurrent() {
        current++;
    }
}
