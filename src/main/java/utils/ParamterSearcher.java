package utils;

public interface ParamterSearcher<V> {

    void partialResult(Boolean condition);

    V next();

    boolean done();

    void reset();
}
