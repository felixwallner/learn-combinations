import at.tugraz.learning.suls.DotMealyMachineParser;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Symbol;
import utils.Eval;
import utils.FileUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Starting...");

        // --------------------
        // Arguments

        boolean recursiveDirectorySearch = true;
        Path inDir = Paths.get("input/mealy/principle");
        Path outDir = Paths.get("out");
        long numberOfSeeds = 10;
        boolean runInParallel = true;

        // --------------------
        // Setup

        System.setProperty("ranked_random.deterministic", "true");

        // --------------------
        // Run

        DotMealyMachineParser symbolStringParser = new DotMealyMachineParser();

        Files.createDirectories(outDir);
        List<Path> dots = FileUtils.getAllDotFiles(inDir, recursiveDirectorySearch);
        // TODO: potentially sort files by size to test smalles first and largest last
        // dots.sort(new Comparator<Path>() {
        //     @Override
        //     public int compare(Path arg0, Path arg1) {
        //         MealyMachine<?, Symbol, ?, String> m1 = symbolStringParser.parse(arg0.toString());
        //         MealyMachine<?, Symbol, ?, String> m2 = symbolStringParser.parse(arg1.toString());
        //         if (m1.getStates().size() > m2.getStates().size()) {
        //             return 1;
        //         } else if (m1.getStates().size() < m2.getStates().size()) {
        //             return -1;
        //         } else {
        //             return 0;
        //         }
        //     }
        // });
        for (Path dot : dots) {
            MealyMachine<?, Symbol, ?, String> symbolMealy = symbolStringParser.parse(dot.toString());

            Eval.runAllCombinationsForLowestParameterCorrectModelAndExportCsvTypeSymbolString(
                    dot.getFileName(),
                    outDir,
                    symbolMealy,
                    (Alphabet<Symbol>) symbolStringParser.getAlphabet(),
                    numberOfSeeds,
                    runInParallel);
        }

        System.out.println("done!");
    }
}
