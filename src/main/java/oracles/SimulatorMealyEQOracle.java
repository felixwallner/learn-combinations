package oracles;

import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.query.DefaultQuery;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.util.automata.Automata;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import javax.annotation.Nullable;
import java.util.Collection;

public class SimulatorMealyEQOracle<I, O> implements EquivalenceOracle.MealyEquivalenceOracle<I, O> {

    private final MealyMachine<?, I, ?, O> reference;
    private final Alphabet<I> alphabet;

    public SimulatorMealyEQOracle(final MealyMachine<?, I, ?, O> reference, final Alphabet<I> alphabet) {
        this.reference = reference;
        this.alphabet = alphabet;
    }

    @Nullable
    @Override
    public DefaultQuery<I, Word<O>> findCounterExample(MealyMachine<?, I, ?, O> hypothesis, Collection<? extends I> inputs) {
        final Word<I> sep = Automata.findSeparatingWord(reference, hypothesis, alphabet);

        if (sep == null) {
            return null;
        }

        return new DefaultQuery<I, Word<O>>(sep, reference.computeOutput(sep));
    }
}