package tracking;

import choices.LearningAlgorithmChoice;
import choices.TestingAlgorithmChoice;
import choices.TestingAlgorithmVariables;
import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.util.Experiment;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.graphs.UniversalGraph;
import net.automatalib.serialization.dot.DOTSerializationProvider;
import net.automatalib.words.Alphabet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

public class StatisticTrackingContainer<I, O> implements StatisticTrackingAccess<I, O>, Callable<Boolean> {
    private final StatisticTrackingSUL<I, O> memQuerySUL;
    private final StatisticTrackingSUL<I, O> eqQuerySUL;
    private final EquivalenceOracle.MealyEquivalenceOracle<I, O> perfectEQOracle;
    private final Alphabet<I> inputAlphabet;
    private final Experiment.MealyExperiment<I, O> experiment;
    private final LearningAlgorithmChoice la;
    private final TestingAlgorithmChoice ta;
    private final TestingAlgorithmVariables taVars;

    public StatisticTrackingContainer(StatisticTrackingSUL<I, O> memQuerySUL,
                                      StatisticTrackingSUL<I, O> eqQuerySUL,
                                      EquivalenceOracle.MealyEquivalenceOracle<I, O> perfectEQOracle,
                                      Alphabet<I> inputAlphabet,
                                      Experiment.MealyExperiment<I, O> experiment,
                                      LearningAlgorithmChoice la,
                                      TestingAlgorithmChoice ta,
                                      TestingAlgorithmVariables taVars) {
        this.memQuerySUL = memQuerySUL;
        this.eqQuerySUL = eqQuerySUL;
        this.perfectEQOracle = perfectEQOracle;
        this.inputAlphabet = inputAlphabet;
        this.experiment = experiment;
        this.la = la;
        this.ta = ta;
        this.taVars = taVars;
    }

    @Override
    public Boolean isLearnedModelCorrect() {
        if (perfectEQOracle.findCounterExample(experiment.getFinalHypothesis(), inputAlphabet) == null) {
            return true;
        }
        return false;
    }

    @Override
    public LearningAlgorithmChoice getLa() {
        return la;
    }

    @Override
    public TestingAlgorithmChoice getTa() {
        return ta;
    }

    @Override
    public Long getMemberQueryTests() {
        return memQuerySUL.getTests().getCount();
    }

    @Override
    public Long getEQQueryTests() {
        return eqQuerySUL.getTests().getCount();
    }

    @Override
    public Long getMemberQuerySteps() {
        return memQuerySUL.getSteps().getCount();
    }

    @Override
    public Long getEQQuerySteps() {
        return eqQuerySUL.getSteps().getCount();
    }

    @Override
    public Long getNumberOfSeeds() {
        return 1L;
    }

    @Override
    public Long getMaxDepth() {
        return taVars.getMaxDepth();
    }

    @Override
    public Long getMaxTests() {
        return taVars.getMaxTests();
    }

    public Long getCombinedTests() {
        return getMemberQueryTests() + getEQQueryTests();
    }

    public Long getCombinedSteps() {
        return getMemberQuerySteps() + getEQQuerySteps();
    }

    public Long getEQQueries() {
        return experiment.getRounds().getCount();
    }

    public void exportDotFinalHypothesis(Path filename) throws IOException {
        final MealyMachine<?, I, ?, O> finalHypothesis = experiment.getFinalHypothesis();
        final UniversalGraph graph = finalHypothesis.transitionGraphView(inputAlphabet);
        DOTSerializationProvider.getInstance().writeModel(
                new File(filename.toString()),
                graph // TODO: unchecked
        );
    }

    public Experiment.MealyExperiment<I, O> getExperiment() {
        return experiment;
    }

//    public double getAverageMembershipQueriesDuringEQQuery() {
//        double result = -1.0;
//        if (eqOracle.getEquivalentQueries().getCount() > 0) {
//            result = memOracle.getMemQueriesDuringEQQuery().getCount() / eqOracle.getEquivalentQueries().getCount();
//        }
//        return result;
//    }


    public TestingAlgorithmVariables getTaVars() {
        return taVars;
    }

    public Long getSeed() {
        Long res = null;
        if (taVars != null) {
            res = taVars.getSeed();
        }
        return res;
    }

    @Override
    public Boolean call() throws Exception {
        System.out.println("Called container with seed " + getSeed() + " as thread " + Thread.currentThread().getId());
        experiment.run();
        System.out.println("FINISHED container with seed " + getSeed() + " as thread " + Thread.currentThread().getId());
        return isLearnedModelCorrect();
    }
}
