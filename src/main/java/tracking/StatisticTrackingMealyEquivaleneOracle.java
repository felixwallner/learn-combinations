package tracking;

import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.filter.statistic.Counter;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.words.Word;

import javax.annotation.Nullable;
import java.util.Collection;

public class StatisticTrackingMealyEquivaleneOracle<I, O> implements EquivalenceOracle.MealyEquivalenceOracle<I, O> {
    private final MealyEquivalenceOracle<I, O> trainingEQOracle;

    /**
     * This membership oracle must be the same one that trainingEQOracle uses for membership queries!
     */
    private final StatisticTrackingMealyMembershipOracle<I, O> sameMemOracle;

    private final Counter equivalentQueries;

    public StatisticTrackingMealyEquivaleneOracle(MealyEquivalenceOracle<I, O> trainingEQOracle, StatisticTrackingMealyMembershipOracle<I, O> sameMemOracle) {
        this.trainingEQOracle = trainingEQOracle;
        this.sameMemOracle = sameMemOracle;
        equivalentQueries = new Counter("equivalence queries", "#");
    }

    @Nullable
    @Override
    public DefaultQuery<I, Word<O>> findCounterExample(MealyMachine<?, I, ?, O> hypothesis, Collection<? extends I> inputs) {
        equivalentQueries.increment();
        sameMemOracle.setEquivalentQueriedATM(true);
        DefaultQuery<I, Word<O>> result = trainingEQOracle.findCounterExample(hypothesis, inputs);
        sameMemOracle.setEquivalentQueriedATM(false);
        return result;
    }

    public Counter getEquivalentQueries() {
        return equivalentQueries;
    }
}
