package tracking;

import choices.LearningAlgorithmChoice;
import choices.TestingAlgorithmChoice;

import java.util.ArrayList;
import java.util.List;

public class StatisticTrackingContainerSummary<I, O> {
    private final List<List<StatisticTrackingAccess<I, O>>> table;
    private final long rows;
    private final long cols;

    public StatisticTrackingContainerSummary() {
        table = new ArrayList<>();
        for (TestingAlgorithmChoice ta : TestingAlgorithmChoice.values()) {
            List<StatisticTrackingAccess<I, O>> row = new ArrayList<>();
            for (LearningAlgorithmChoice la : LearningAlgorithmChoice.values()) {
                row.add(null);
            }
            table.add(row);
        }
        rows = TestingAlgorithmChoice.values().length;
        cols = LearningAlgorithmChoice.values().length;
    }

    public void add(List<StatisticTrackingContainer<I, O>> containerSeeds) {
        StatisticTrackingAccess<I, O> average = new StatisticTrackingAverageSeeds<>(containerSeeds);
        set(average.getLa(), average.getTa(), average);
    }

    public void add(StatisticTrackingContainer<I, O> container) {
        set(container.getLa(), container.getTa(), container);
    }

    public StatisticTrackingAccess<I, O> get(LearningAlgorithmChoice la, TestingAlgorithmChoice ta) {
        return table.get(ta.ordinal()).get(la.ordinal());
    }

//    public void exportDotAllFinalHypothesesTable(Path basefilename) throws IOException {
//        for (TestingAlgorithmChoice ta : TestingAlgorithmChoice.values()) {
//            for (LearningAlgorithmChoice la : LearningAlgorithmChoice.values()) {
//                Path filename = basefilename.resolveSibling(basefilename.getFileName() + "-" + ta.name() + "-" + la.name());
//                get(la, ta).exportDotFinalHypothesis(filename);
//            }
//        }
//    }

    private void set(LearningAlgorithmChoice la, TestingAlgorithmChoice ta,
                     StatisticTrackingAccess<I, O> info) {
        if (get(la, ta) != null) {
            System.out.println("WARNING: Experiment slot already filled. Overwriting!");
        }
        table.get(ta.ordinal()).set(la.ordinal(), info);
    }

    public boolean isRowEmpty(TestingAlgorithmChoice ta) {
        for (StatisticTrackingAccess<I, O> info : table.get(ta.ordinal())) {
            if (info != null) {
                return false;
            }
        }
        return true;
    }

    public long getRows() {
        return rows;
    }

    public long getCols() {
        return cols;
    }
}
