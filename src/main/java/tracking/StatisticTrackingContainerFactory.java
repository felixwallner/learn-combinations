package tracking;

import at.tugraz.mutation_equiv.RandomCoverageSelectionEQOracle;
import at.tugraz.mutation_equiv.TestTrackingSUL;
import at.tugraz.mutation_equiv.mutation.ChangeOutputOperator;
import at.tugraz.mutation_equiv.mutation.MutationOperator;
import at.tugraz.mutation_equiv.mutation.SplitStateOperator;
import at.tugraz.mutation_equiv.mutation.sampling.MutantSamplingStrategy;
import at.tugraz.mutation_equiv.mutation.sampling.impl.*;
import at.tugraz.mutation_equiv.test_selection.MutationSuiteBasedSelector;
import at.tugraz.mutation_equiv.test_selection.TestSelector;
import at.tugraz.mutation_equiv.trace_gen.MuDirectedIterativeGenerator;
import at.tugraz.mutation_equiv.trace_gen.TraceGenerator;
import choices.LearningAlgorithmChoice;
import choices.TestingAlgorithmChoice;
import choices.TestingAlgorithmVariables;
import de.learnlib.algorithms.adt.learner.ADTLearner;
import de.learnlib.algorithms.adt.learner.ADTLearnerBuilder;
import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealyBuilder;
import de.learnlib.algorithms.lstar.mealy.ExtensibleLStarMealyBuilder;
import de.learnlib.algorithms.rivestschapire.RivestSchapireMealyBuilder;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealyBuilder;
import de.learnlib.api.SUL;
import de.learnlib.api.algorithm.LearningAlgorithm;
import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.driver.util.MealySimulatorSUL;
import de.learnlib.filter.cache.sul.SULCaches;
import de.learnlib.oracle.equivalence.RandomWordsEQOracle;
import de.learnlib.oracle.equivalence.RandomWpMethodEQOracle;
import de.learnlib.oracle.equivalence.WMethodEQOracle;
import de.learnlib.oracle.equivalence.WpMethodEQOracle;
import de.learnlib.oracle.membership.SULOracle;
import de.learnlib.oracle.membership.SULSymbolQueryOracle;
import de.learnlib.util.Experiment;
import learners.ActiveRPNILearner;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Symbol;
import oracles.MaxStepsRandomWalkEQOracle;
import oracles.SimulatorMealyEQOracle;
import tracking.StatisticTrackingContainer;
import tracking.StatisticTrackingSUL;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Math.max;
import static java.lang.Math.toIntExact;

public class StatisticTrackingContainerFactory<I, O> {
        private final MealyMachine<?, I, ?, O> fm;
        private final Class<I> inputTypeParameterClass;
        private final Class<O> outputTypeParameterClass;
        private final SUL<I, O> sul;
        private final Alphabet<I> inputAlphabet;

        public StatisticTrackingContainerFactory(
                        MealyMachine<?, I, ?, O> fm,
                        Class<I> inputTypeParameterClass,
                        Class<O> outputTypeParameterClass,
                        Alphabet<I> inputAlphabet) {

                this.fm = fm;
                this.inputTypeParameterClass = inputTypeParameterClass;
                this.outputTypeParameterClass = outputTypeParameterClass;
                this.inputAlphabet = inputAlphabet;
                MealySimulatorSUL<I, O> pureSUL = new MealySimulatorSUL<>(fm);
                this.sul = pureSUL; // SULCaches.createCache(this.inputAlphabet, pureSUL);
        }

        public StatisticTrackingContainer<I, O> getExperimentHolder(
                        LearningAlgorithmChoice learningAlgorithmChoice,
                        TestingAlgorithmChoice testingAlgorithmChoice,
                        TestingAlgorithmVariables testingVars) {

                if (learningAlgorithmChoice == null || testingAlgorithmChoice == null) {
                        throw new IllegalArgumentException("Argument is null");
                }

                /**
                 * Build two different STsuls from the same caching SUL, so that member and eq
                 * queries can be tracked separately
                 */
                StatisticTrackingSUL<I, O> memQuerySUL = new StatisticTrackingSUL<>(this.sul);
                StatisticTrackingSUL<I, O> eqQuerySUL = new StatisticTrackingSUL<>(this.sul);

                /**
                 * Use membership oracle to build a learning algorithm based on
                 * learningAlgorithmChoice
                 */
                LearningAlgorithm.MealyLearner<I, O> learningAlgorithm = buildLearningAlgorithm(memQuerySUL,
                                learningAlgorithmChoice);

                /**
                 * Use a different STsul to track equivalent queries (may use the same
                 * underlying SULCache)
                 * Build an eqOracle with given testingAlgorithm
                 */
                EquivalenceOracle.MealyEquivalenceOracle<I, O> eqOracle = buildTrainingEQOracle(eqQuerySUL,
                                testingAlgorithmChoice, testingVars);

                /**
                 * Build new experiment using the learning algorithm (memQuerySUL), eqOracle
                 * (eqQuerySUL) and alphabet
                 */
                Experiment.MealyExperiment<I, O> experiment = new Experiment.MealyExperiment<I, O>(learningAlgorithm,
                                eqOracle, this.inputAlphabet);

                /**
                 * Build a "perfect" eq oracle that can answer, if the model and the actual
                 * automaton are identical
                 * This eq oracle is NOT used for training and ONLY for the final question, if
                 * the learned model is correct
                 * It is "perfect" because it knows the automaton behind the SUL and can simply
                 * find a separating word
                 */
                EquivalenceOracle.MealyEquivalenceOracle<I, O> perfectEQOracle = new SimulatorMealyEQOracle<I, O>(
                                this.fm, this.inputAlphabet);

                return new StatisticTrackingContainer<I, O>(
                                memQuerySUL,
                                eqQuerySUL,
                                perfectEQOracle,
                                inputAlphabet,
                                experiment,
                                learningAlgorithmChoice,
                                testingAlgorithmChoice,
                                new TestingAlgorithmVariables(testingVars));
        }

        private LearningAlgorithm.MealyLearner<I, O> buildLearningAlgorithm(SUL<I, O> memQuerySUL,
                        LearningAlgorithmChoice la) {

                MembershipOracle.MealyMembershipOracle<I, O> memOracle = new SULOracle<>(memQuerySUL);
                LearningAlgorithm.MealyLearner<I, O> learner;
                switch (la) {
                        case LSTAR:
                                learner = new ExtensibleLStarMealyBuilder<I, O>()
                                                .withAlphabet(inputAlphabet)
                                                .withOracle(memOracle)
                                                .create();
                                break;
                        case LSTAR_RS:
                                learner = new RivestSchapireMealyBuilder<I, O>()
                                                .withAlphabet(inputAlphabet)
                                                .withOracle(memOracle)
                                                .create();
                                break;
                        case TTT:
                                learner = new TTTLearnerMealyBuilder<I, O>()
                                                .withAlphabet(inputAlphabet)
                                                .withOracle(memOracle)
                                                .create();
                                break;
                        case KV:
                                learner = new KearnsVaziraniMealyBuilder<I, O>()
                                                .withAlphabet(inputAlphabet)
                                                .withOracle(memOracle)
                                                .create();
                                break;
                        case RPNI:
                                learner = new ActiveRPNILearner<>(inputAlphabet, memOracle);
                                break;
                        case ADT:
                                SULSymbolQueryOracle<I, O> symbolOracle = new SULSymbolQueryOracle<I, O>(memQuerySUL);
                                learner = new ADTLearnerBuilder<I, O>()
                                                .withAlphabet(inputAlphabet)
                                                .withOracle(symbolOracle)
                                                .create();
                                break;
                        default:
                                throw new UnsupportedOperationException("Learning Algorithm " + la.name()
                                                + " not implemented.");
                }
                return learner;
        }

        private EquivalenceOracle.MealyEquivalenceOracle<I, O> buildTrainingEQOracle(SUL<I, O> eqQuerySUL,
                        TestingAlgorithmChoice ta, TestingAlgorithmVariables testingVars) {

                MembershipOracle.MealyMembershipOracle<I, O> eqMemOracle = new SULOracle<>(eqQuerySUL);
                EquivalenceOracle.MealyEquivalenceOracle<I, O> trainingEQOracle;
                switch (ta) {
                        case W_METHOD:
                                trainingEQOracle = new WMethodEQOracle.MealyWMethodEQOracle<I, O>(
                                                eqMemOracle,
                                                toIntExact(testingVars.getMaxDepth()) // max depth (number of states
                                                                                      // found)
                                );
                                break;
                        case WP_METHOD:
                                trainingEQOracle = new WpMethodEQOracle.MealyWpMethodEQOracle<I, O>(
                                                eqMemOracle,
                                                toIntExact(testingVars.getMaxDepth()) // max depth (number of states
                                                                                      // found)
                                );
                                break;
                        case RANDOM_WP_METHOD:
                                trainingEQOracle = new RandomWpMethodEQOracle.MealyRandomWpMethodEQOracle<I, O>(
                                                eqMemOracle,
                                                toIntExact(testingVars.getRandomWPMinimalLength()), // minimal size of
                                                                                                    // the random word
                                                toIntExact(testingVars.getRandomWPExpectedLength()), // expected length
                                                                                                     // of random word
                                                toIntExact(testingVars.getMaxTests()), // bound == max # of tests (0 is
                                                                                       // unbound; doesn't terminate)
                                                new Random(testingVars.getSeed()),
                                                1);
                                break;
                        case RANDOM_WALKS:
                                trainingEQOracle = new MaxStepsRandomWalkEQOracle<I, O>(
                                                eqQuerySUL,
                                                testingVars.getRandomWalkRestartProbability(),
                                                testingVars.getMaxTests(),
                                                new Random(testingVars.getSeed()));
                                break;
                        case RANDOM_WORDS:
                                trainingEQOracle = new RandomWordsEQOracle.MealyRandomWordsEQOracle<I, O>(
                                                eqMemOracle,
                                                toIntExact(testingVars.getRandomWordsMinimalLength()),
                                                toIntExact(testingVars.getRandomWordsMaximumLength()),
                                                toIntExact(testingVars.getMaxTests()),
                                                new Random(testingVars.getSeed()));
                                break;
                        case MUTATION: {
                                if (inputTypeParameterClass != Symbol.class) {
                                        throw new UnsupportedOperationException("Testing Algorithm " + ta.name()
                                                        + " requires InputType 'Symbol'.");
                                }
                                if (outputTypeParameterClass != String.class) {
                                        throw new UnsupportedOperationException("Testing Algorithm " + ta.name()
                                                        + " requires OutputType 'String'.");
                                }
                                List<MutationOperator> mutationoperatorList = new ArrayList<MutationOperator>();
                                SplitStateOperator splitState = new SplitStateOperator(
                                                (Alphabet<Symbol>) inputAlphabet,
                                                toIntExact(testingVars.getMutationSplitStateDepth()));
                                splitState.setAccSeqBound(toIntExact(testingVars.getMutationSplitStateAccSeqBound()));
                                splitState.setAllowDiffInLastAccSymbol(
                                                testingVars.isMutationSplitStateAllowDifLastAcc());
                                splitState.setAllowEqualPreStateAndSymbol(
                                                testingVars.isMutationSplitStateAllowEqualPreState());
                                mutationoperatorList.add(splitState);
                                mutationoperatorList.add(new ChangeOutputOperator((Alphabet<Symbol>) inputAlphabet));
                                TestSelector testSelector = new MutationSuiteBasedSelector(
                                                toIntExact(testingVars.getMaxTests()),
                                                (Alphabet<Symbol>) inputAlphabet,
                                                testingVars.isMutationTestSelectorKillAliveMutants(),
                                                false);
                                TraceGenerator traceGenerator = new MuDirectedIterativeGenerator(
                                                testingVars.getMutationTraceGenStopProb(),
                                                testingVars.getMutationTraceGenRetryProb(),
                                                (Alphabet<Symbol>) inputAlphabet,
                                                toIntExact(testingVars.getMutationTraceGenMaxTestLength()),
                                                toIntExact(testingVars.getMutationTraceGenMaxRandWordLen()));
                                traceGenerator.updateRandomSeed(testingVars.getSeed());
                                MutantSamplingStrategy mutantSamplingStrategy = new CompositionSampler(
                                                new ReduceToMinSampler(new Random(testingVars.getSeed()),
                                                                "split-state-state"),
                                                new ElementBasedFractionSampler(1, new Random(testingVars.getSeed()),
                                                                "operator:split-state-1"),
                                                new ElementBasedBoundSampler(10000, new Random(testingVars.getSeed()),
                                                                "operator:split-state-1"));
                                mutantSamplingStrategy.updateSeed(testingVars.getSeed());
                                MutantSamplingStrategy mutantGenSamplingStrategy = new ElementBasedBoundSampler(
                                                0, new Random(testingVars.getSeed()), "operator:split-state-1");
                                mutantGenSamplingStrategy.updateSeed(testingVars.getSeed());

                                RandomCoverageSelectionEQOracle tEQOracle = new RandomCoverageSelectionEQOracle(
                                                new TestTrackingSUL((SUL<Symbol, String>) eqQuerySUL),
                                                (Alphabet<Symbol>) inputAlphabet,
                                                mutationoperatorList,
                                                Math.min(toIntExact(
                                                                testingVars.getMutationTestSelectionSuiteSizeFactor()
                                                                                * testingVars.getMaxTests()),
                                                                100000),
                                                testingVars.isMutationTestSelectionReuseRemaining(),
                                                testSelector,
                                                traceGenerator,
                                                mutantSamplingStrategy);
                                tEQOracle.setMutantGenerationSampler(mutantGenSamplingStrategy);
                                trainingEQOracle = (EquivalenceOracle.MealyEquivalenceOracle<I, O>) tEQOracle;
                        }
                                break;
                        case TRANSITION_COVERAGE: {
                                if (inputTypeParameterClass != Symbol.class) {
                                        throw new UnsupportedOperationException("Testing Algorithm " + ta.name()
                                                        + " requires InputType 'Symbol'.");
                                }
                                if (outputTypeParameterClass != String.class) {
                                        throw new UnsupportedOperationException("Testing Algorithm " + ta.name()
                                                        + " requires OutputType 'String'.");
                                }
                                List<MutationOperator> mutationoperatorList = new ArrayList<MutationOperator>();
                                mutationoperatorList.add(new ChangeOutputOperator((Alphabet<Symbol>) inputAlphabet));
                                TestSelector testSelector = new MutationSuiteBasedSelector(
                                                toIntExact(testingVars.getMaxTests()),
                                                (Alphabet<Symbol>) inputAlphabet,
                                                testingVars.isTransitionCoverageTestSelectorKillAliveMutants(),
                                                false);
                                TraceGenerator traceGenerator = new MuDirectedIterativeGenerator(
                                                testingVars.getTransitionCoverageTraceGenStopProb(),
                                                testingVars.getTransitionCoverageTraceGenRetryProb(),
                                                (Alphabet<Symbol>) inputAlphabet,
                                                toIntExact(testingVars.getTransitionCoverageTraceGenMaxTestLength()),
                                                toIntExact(testingVars.getTransitionCoverageTraceGenMaxRandWordLen()));
                                traceGenerator.updateRandomSeed(testingVars.getSeed());
                                MutantSamplingStrategy mutantSamplingStrategy = new IdentitySampler();
                                mutantSamplingStrategy.updateSeed(testingVars.getSeed());

                                RandomCoverageSelectionEQOracle tEQOracle = new RandomCoverageSelectionEQOracle(
                                                new TestTrackingSUL((SUL<Symbol, String>) eqQuerySUL),
                                                (Alphabet<Symbol>) inputAlphabet,
                                                mutationoperatorList,
                                                Math.min(toIntExact(testingVars
                                                                .getTransitionCoverageTestSelectionSuiteSizeFactor()
                                                                * testingVars.getMaxTests()), 150000),
                                                testingVars.isTransitionCoverageTestSelectionReuseRemaining(),
                                                testSelector,
                                                traceGenerator,
                                                mutantSamplingStrategy);
                                trainingEQOracle = (EquivalenceOracle.MealyEquivalenceOracle<I, O>) tEQOracle;
                        }
                                break;
                        default:
                                throw new UnsupportedOperationException("Testing Algorithm " + ta.name()
                                                + " not implemented.");
                }
                return trainingEQOracle;
        }
}
