package tracking;

import choices.LearningAlgorithmChoice;
import choices.TestingAlgorithmChoice;

import java.util.List;

public class StatisticTrackingAverageSeeds<I, O> implements StatisticTrackingAccess<I, O> {
    private final LearningAlgorithmChoice la;
    private final TestingAlgorithmChoice ta;
    private final Double avgMemberTests;
    private final Double avgEQTests;
    private final Double avgMemberSteps;
    private final Double avgEQSteps;
    private final Long numberOfSeeds;
    private final Double avgMaxDepth;
    private final Double avgMaxTests;
    private final Boolean allModelsCorrect;

    public StatisticTrackingAverageSeeds(List<StatisticTrackingContainer<I, O>> containerSeeds) {
        if (containerSeeds == null || containerSeeds.isEmpty()) {
            la = null;
            ta = null;
            avgMemberTests = 0.0;
            avgEQTests = 0.0;
            avgMemberSteps = 0.0;
            avgEQSteps = 0.0;
            numberOfSeeds = 0L;
            avgMaxDepth = 0.0;
            avgMaxTests = 0.0;
            allModelsCorrect = false;
        } else {
            la = containerSeeds.get(0).getLa();
            ta = containerSeeds.get(0).getTa();
            numberOfSeeds = (long) containerSeeds.size();
            long avgMemberTests = 0;
            long avgEQTests = 0;
            long avgMemberSteps = 0;
            long avgEQSteps = 0;
            long avgMaxDepth = 0;
            long avgMaxTests = 0;
            Boolean modelsCorrect = true;
            for (StatisticTrackingContainer<I, O> con : containerSeeds) {
                if (con.getLa() != la || con.getTa() != ta) {
                    throw new IllegalArgumentException("Added list must have the same Learning and Testing algorithm " +
                            "and may only be different in seed.");
                }
                Boolean conModelCorrect = con.isLearnedModelCorrect();
                if (conModelCorrect == null) {
                    modelsCorrect = null;
                } else if (modelsCorrect == true && !conModelCorrect) {
                    modelsCorrect = false;
                }
                avgMemberTests += con.getMemberQueryTests();
                avgEQTests += con.getEQQueryTests();
                avgMemberSteps += con.getMemberQuerySteps();
                avgEQSteps += con.getEQQuerySteps();
                avgMaxDepth += con.getMaxDepth();
                avgMaxTests += con.getMaxTests();
            }
            this.avgMemberTests = ((double)avgMemberTests / (double)numberOfSeeds);
            this.avgEQTests = ((double)avgEQTests / (double)numberOfSeeds);
            this.avgMemberSteps = ((double)avgMemberSteps / (double)numberOfSeeds);
            this.avgEQSteps = ((double)avgEQSteps / (double)numberOfSeeds);
            this.avgMaxDepth = ((double)avgMaxDepth / (double)numberOfSeeds);
            this.avgMaxTests = ((double)avgMaxTests / (double)numberOfSeeds);
            this.allModelsCorrect = modelsCorrect;
        }
    }

    @Override
    public LearningAlgorithmChoice getLa() {
        return la;
    }

    @Override
    public TestingAlgorithmChoice getTa() {
        return ta;
    }

    @Override
    public Long getMemberQueryTests() {
        return Math.round(avgMemberTests);
    }

    @Override
    public Long getEQQueryTests() {
        return Math.round(avgEQTests);
    }

    @Override
    public Long getMemberQuerySteps() {
        return Math.round(avgMemberSteps);
    }

    @Override
    public Long getEQQuerySteps() {
        return Math.round(avgEQSteps);
    }

    @Override
    public Long getNumberOfSeeds() {
        return numberOfSeeds;
    }

    @Override
    public Long getMaxDepth() {
        return Math.round(avgMaxDepth);
    }

    @Override
    public Long getMaxTests() {
        return Math.round(avgMaxTests);
    }

    @Override
    public Boolean isLearnedModelCorrect() {
        return allModelsCorrect;
    }
}
