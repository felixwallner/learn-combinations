package tracking;

import de.learnlib.api.SUL;
import de.learnlib.api.exception.SULException;
import de.learnlib.filter.statistic.Counter;

import javax.annotation.Nullable;

public class StatisticTrackingSUL<I, O> implements SUL<I, O> {
    private final SUL<I, O> actualSUL;
    private final Counter steps;
    private final Counter tests;

    public StatisticTrackingSUL(SUL<I, O> actualSUL) {
        this.actualSUL = actualSUL;
        steps = new Counter("Steps", "#");
        tests = new Counter("Tests", "#");
    }

    /**
     * setup SUL.
     */
    @Override
    public void pre() {
        actualSUL.pre();
    }

    /**
     * shut down SUL.
     */
    @Override
    public void post() {
        tests.increment();
        actualSUL.post();
    }

    /**
     * make one step on the SUL.
     *
     * @param in input to the SUL
     * @return output of SUL
     */
    @Nullable
    @Override
    public O step(@Nullable I in) throws SULException {
        steps.increment();
        return actualSUL.step(in);
    }

    public Counter getSteps() {
        return steps;
    }

    public Counter getTests() {
        return tests;
    }
}
