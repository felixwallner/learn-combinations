package tracking;

import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.api.query.Query;
import de.learnlib.filter.statistic.Counter;
import net.automatalib.words.Word;

import java.util.Collection;

public class StatisticTrackingMealyMembershipOracle<I, O> implements MembershipOracle.MealyMembershipOracle<I, O> {
    private final MealyMembershipOracle<I, O> memOracle;
    private final Counter memQueries;
    private final Counter memQueriesDuringEQQuery;

    /**
     * This variable must be set and unset from outside during and after an equivalent query
     * with this membershipOracle respectively.
     * It should be true if this membershipOracle is used for an equivalent query At The Moment, and false otherwise.
     * Membership queries are then counted to a different counter.
     */
    private boolean isEquivalentQueriedATM = false;

    private void incrementCounter(long num) {
        if (isEquivalentQueriedATM) {
            memQueriesDuringEQQuery.increment(num);
        } else {
            memQueries.increment(num);
        }
    }

    public StatisticTrackingMealyMembershipOracle(MealyMembershipOracle<I, O> memOracle) {
        this.memOracle = memOracle;
        memQueries = new Counter("membership queries outside of EQ query", "#");
        memQueriesDuringEQQuery = new Counter("membership queries during of EQ query", "#");;
    }

    public Counter getCombinedMemQueries() {
        Counter result = new Counter("combined membership queries", "#");
        result.increment(memQueries.getCount() + memQueriesDuringEQQuery.getCount());
        return result;
    }

    @Override
    public Word<O> answerQuery(Word<I> input) {
        return memOracle.answerQuery(input);
    }

    @Override
    public Word<O> answerQuery(Word<I> prefix, Word<I> suffix) {
        return memOracle.answerQuery(prefix, suffix);
    }

    /**
     * Processes a single query. When this method returns, the {@link Query#answer(Object)} method of the supplied
     * object will have been called with an argument reflecting the SUL response to the respective query.
     * <p>
     * The default implementation of this method will simply wrap the provided {@link Query} in a singleton {@link
     * Collection} using {@link Collections#singleton(Object)}. Implementations in subclasses should override this
     * method to circumvent the Collection object creation, if possible.
     *
     * @param query the query to process
     */
    @Override
    public void processQuery(Query<I, Word<O>> query) {
        incrementCounter(1);
        memOracle.processQuery(query);
    }

    /**
     * Processes the specified collection of queries. When this method returns, each of the contained queries {@link
     * Query#answer(Object)} method should have been called with an argument reflecting the SUL response to the
     * respective query.
     *
     * @param queries the queries to process
     * @see Query#answer(Object)
     */
    @Override
    public void processQueries(Collection<? extends Query<I, Word<O>>> queries) {
        incrementCounter(queries.size());
        memOracle.processQueries(queries);
    }

    public Counter getMemQueries() {
        return memQueries;
    }

    public Counter getMemQueriesDuringEQQuery() {
        return memQueriesDuringEQQuery;
    }

    public boolean isEquivalentQueriedATM() {
        return isEquivalentQueriedATM;
    }

    public void setEquivalentQueriedATM(boolean equivalentQueriedATM) {
        isEquivalentQueriedATM = equivalentQueriedATM;
    }
}
