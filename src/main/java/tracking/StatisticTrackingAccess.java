package tracking;

import choices.LearningAlgorithmChoice;
import choices.TestingAlgorithmChoice;

public interface StatisticTrackingAccess<I, O> {

    LearningAlgorithmChoice getLa();

    TestingAlgorithmChoice getTa();

    Long getMemberQueryTests();

    Long getEQQueryTests();

    Long getMemberQuerySteps();

    Long getEQQuerySteps();

    Long getNumberOfSeeds();

    Long getMaxDepth();

    Long getMaxTests();

    Boolean isLearnedModelCorrect();
}
