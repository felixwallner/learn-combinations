package learners;

import de.learnlib.algorithms.rpni.BlueFringeRPNIMealy;
import de.learnlib.api.algorithm.LearningAlgorithm;
import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.api.query.DefaultQuery;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.commons.util.collections.CollectionsUtil;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import javax.annotation.Nonnull;
import java.util.*;

public class ActiveRPNILearner<I, O> implements LearningAlgorithm.MealyLearner<I, O> {
    private final int nrInputCombinations;
    private MealyMachine<?, I, ?, O> currentModel;

    private static class CacheNode<I>{
        private Map<I,CacheNode<I>> children = new HashMap<>();

        public CacheNode<I> getOrAddChild(I input) {
            if(children.containsKey(input))
                return children.get(input);
            else{
                CacheNode<I> newNode = new CacheNode<>();
                children.putIfAbsent(input,newNode);
                return newNode;
            }
        }

        public CacheNode<I> getChild(I input) {
            return children.get(input);
        }
    }
    private static class InputTreeCache<I>{
        private CacheNode<I> root = null;
        public InputTreeCache(){
            root = new CacheNode<>();
        }
        public void add(Word<I> word){
            CacheNode<I> currNode = root;
            for (I input : word){
                currNode = currNode.getOrAddChild(input);
            }
        }
        public boolean contains(Word<I> word){
            CacheNode<I> currNode = root;
            for (I input : word){
                currNode = currNode.getChild(input);
                if(currNode == null)
                    return false;
            }
            return true;
        }
    }


    private final Alphabet<I> inputAlphabet;
    private final MembershipOracle.MealyMembershipOracle<I, O> memOracle;
    private final BlueFringeRPNIMealy<I, O> learner;
    private InputTreeCache<I> testedSeqCache = new InputTreeCache<>();

    public ActiveRPNILearner(Alphabet<I> inputAlphabet, MembershipOracle.MealyMembershipOracle<I, O> memOracle) {
        this(inputAlphabet,memOracle,2);
    }
    public ActiveRPNILearner(Alphabet<I> inputAlphabet, MembershipOracle.MealyMembershipOracle<I, O> memOracle,
                             int nrInputCombinations) {
        this.inputAlphabet = inputAlphabet;
        this.memOracle = memOracle;
        this.learner = new BlueFringeRPNIMealy<I,O>(inputAlphabet);
        this.nrInputCombinations = nrInputCombinations;
        staticRoundĆounter = 0;
    }

    @Override
    public void startLearning() {
        Iterable<List<I>> inputCombinations = CollectionsUtil.allTuples(inputAlphabet, nrInputCombinations, nrInputCombinations);
        for(List<I> inputList : inputCombinations){
            Word<I> inputAsWord = Word.fromList(inputList);
            Word<O> response = memOracle.answerQuery(inputAsWord);
            learner.addSample(inputAsWord, response);
            testedSeqCache.add(inputAsWord);
        }
    }

    @Override
    public boolean refineHypothesis(@Nonnull DefaultQuery<I, Word<O>> cex) {
        Word<I> cexInputPart = cex.getInput();
        Word<O> outputHyp = currentModel.computeOutput(cexInputPart);
        if (cex.getOutput().equals(outputHyp))
            return false;
        cex = computeCexPrefixToDiff(cex, (MealyMachine<Object, I, ?, O>) currentModel);

        learner.addSample(cex);
        for(Word<I> cexPrefix : cexInputPart.prefixes(false)) {
            Iterable<List<I>> inputCombinations = CollectionsUtil.allTuples(inputAlphabet, nrInputCombinations, nrInputCombinations);
            for(List<I> inputList : inputCombinations){
                Word<I> inputCombination = Word.fromList(inputList);
                Word<I> extendedCexPrefix = cexPrefix.concat(inputCombination);
                if (!testedSeqCache.contains(extendedCexPrefix)) {
                    Word<O> response = memOracle.answerQuery(extendedCexPrefix);
                    learner.addSample(extendedCexPrefix, response);
                    testedSeqCache.add(extendedCexPrefix);
                }
            }
        }
        return true;
    }

    private DefaultQuery<I, Word<O>> computeCexPrefixToDiff(DefaultQuery<I, Word<O>> cex, MealyMachine<Object, I, ?, O> currentModel) {
        Word<I> cexInput = cex.getInput();
        Word<O> cexOutput = cex.getOutput();
        Object currState = currentModel.getInitialState();
        for(int j = 0; j < cexInput.length(); j++){
            I input = cexInput.getSymbol(j);
            O hypOutput = currentModel.getOutput(currState, input);
            O sulOutput = cexOutput.getSymbol(j);
            currState = currentModel.getSuccessor(currState,input);
            if(!hypOutput.equals(sulOutput)){
                return new DefaultQuery<>(cexInput.prefix(j+1),cexOutput.prefix(j+1));
            }
        }
        return null;
    }

    static int staticRoundĆounter = 0;
    @Nonnull
    @Override
    public MealyMachine<?, I, ?, O> getHypothesisModel() {
        currentModel = learner.computeModel();
        System.out.printf("Round %d, with model size %d%n",staticRoundĆounter++,currentModel.getStates().size());
        return currentModel;
    }
}
