import argparse
import os
from pathlib import Path

from aalpy.automata import MealyMachine
from aalpy.utils import load_automaton_from_file
from hardness import calculate_hardness_score
from tqdm import tqdm


def find_all_files_with_suffix(path, suffix, depth=None):
    """Returns a set of relative paths of files ending with suffix.
    If depth is an int, only go <depth> directly levels deeper"""
    files = set()
    for r, d, f in os.walk(path):
        if depth is not None and r.count(os.sep) > depth:
            print(f"INFO: Ignoring files in depth {r.count(os.sep)}:", r)
            continue
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def has_sinkstate(machine: MealyMachine):
    alpha = machine.get_input_alphabet()
    for state in machine.states:
        selfts = state.get_same_state_transitions()
        assert len(selfts) <= len(alpha)
        if len(alpha) == len(selfts):
            return True
    return False


def classify_file(path, easy=1e8, hard=1e12, small=15):
    machine: MealyMachine = load_automaton_from_file(path, "mealy")
    name = Path(path).stem
    classifications = ["small" if len(machine.states) <= small else "large"]
    if machine.is_strongly_connected():
        classifications.append("strongly-connected")
    if has_sinkstate(machine):
        classifications.append("sink-state")
    effort = calculate_hardness_score(machine)
    if effort < easy:
        classifications.append(f"easy")
    elif effort < hard:
        classifications.append(f"medium-hard")
    else:
        classifications.append(f"hard")
    result = f"{name}: {','.join(classifications)}"
    return result


def parseargs():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--path",
        help="Directory path to read dot files from. Defaults to 'input/mealy/principle'.",
        default="input/mealy/principle",
        type=str,
    )
    parser.add_argument(
        "-o",
        "--outfile",
        help="Output file with classifications. Defaults to 'classification.txt'.",
        default="classification.txt",
        type=str,
    )
    parser.add_argument(
        "-dd",
        "--dirdepth",
        help="Directoy depth in which to search for input files.",
        default=None,
        type=int,
    )
    return parser.parse_args()


def main() -> int:
    args = parseargs()
    paths = find_all_files_with_suffix(args.path, ".dot", args.dirdepth)
    if not paths:
        print(f"ERROR: No .dot files found in {args.path}")
        return -1

    result = [classify_file(path) for path in tqdm(paths)]
    result.sort()
    with open(args.outfile, "w") as f:
        f.write("\n".join(result))

    return 0


if __name__ == "__main__":
    exit(main())
