import itertools
import sys

try:
    import matplotlib
    import seaborn as sns

    usecmap = "rocket_r"
except ImportError:
    print("WARN: seaborn is not installed, choosing different cmap")
    usecmap = "OrRd"


import matplotlib.pyplot as plt
import numpy as np
from tikzplotlib import save as tikz_save

# will add root to allow imports from stat (only required if called from root)
sys.path.append(".")

from stats import Experiment, collect_experiment_filesnames, parseargs


def plot_average_stat_of_combination_heatmap(
    exps, category, stat, sorted_normalized_results, outdir, tex=False, prefix=""
):
    result = sorted_normalized_results
    las = sorted(set([e.la for e in exps]))
    las_order = ["LSTAR", "LSTARRS", "KV", "TTT", "ADT"]
    las = [la for la in las_order + sorted(set(las).difference(las_order)) if la in las]
    tas_order = [
        "WMETHOD",
        "WPMETHOD",
        "RANDOMWPMETHOD",
        "RANDOMWALKS",
        "RANDOMWORDS",
        "MUTATION",
        "TRANSITIONCOVERAGE",
    ]
    tas = sorted(set([e.ta for e in exps]))
    tas = [ta for ta in tas_order + sorted(set(tas).difference(tas_order)) if ta in tas]
    # tas = ["WMETHOD", "WPMETHOD", "RANDOMWPMETHOD"] # only for hard
    names = sorted(set([e.filename for e in exps]))
    h = []
    h_not = []
    for ta in tas:
        row = []
        row_not = []
        h.append(row)
        h_not.append(row_not)
        for la in las:
            key = f"{la}_{ta}"
            val = next(r[1] for r in result if r[0] == key)
            row.append(val)
            num_exps = next(len(r[2]) for r in result if r[0] == key)
            if tex:
                row_not.append(
                    f"\\shortstack{{${val:.3f}$\\\\\\text{{\\normalsize$({num_exps}/{len(names)})$}}}}"
                )
            else:
                row_not.append(f"{val:.3f} ({num_exps}/{len(names)})")
            # \shortstack{$0.226$\\\text{\normalsize$(142/153)$}}

    filename = f"norm_avg_{stat}_{category}_heatmap.tex"
    if prefix:
        filename = prefix + "_" + filename
    filename = filename.replace(" ", "").replace("_", "-")
    savedloc = outdir + "/" + filename
    plt.clf()
    fig, ax = plt.subplots()
    im, cbar = heatmap(np.array(h), tas, las, ax=ax, cmap=usecmap)
    texts = annotate_heatmap(im, annot=np.array(h_not), valfmt="")

    fig.tight_layout()
    if not tex:
        fig.set_size_inches(18.5, 10.5)
        plt.savefig(savedloc[:-4] + ".png")
        plt.show()
        plt.close(fig)
        return

    tikz_save(savedloc, figure=fig, strict=True)
    print("INFO: Saved to", savedloc)
    with open(savedloc, "r") as f:
        file = f.read()
    imagename = filename[:-4]
    for old, new in {
        imagename: "content/figures/" + imagename,
        "text=": "font=\\fontsize{15}{0},\n  text=",
        "LSTARRS": "RS",
        "LSTAR": "L$^*$",
        "RANDOMWPMETHOD": "random Wp",
        "WMETHOD": "W-method",
        "WPMETHOD": "Wp-method",
        "RANDOMWALKS": "random walks",
        "RANDOMWORDS": "random words",
        "MUTATION": "mutation",
        "TRANSITIONCOVERAGE": "transition c.",
    }.items():
        file = file.replace(old, new)
    with open(savedloc, "w") as f:
        f.write(file)


def heatmap(
    data,
    row_labels,
    col_labels,
    ax=None,
    cbar_kw=None,
    cbarlabel="",
    rotatexlabels=False,
    **kwargs,
):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (M, N).
    row_labels
        A list or array of length M with the labels for the rows.
    col_labels
        A list or array of length N with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if ax is None:
        ax = plt.gca()

    if cbar_kw is None:
        cbar_kw = {}

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # Show all ticks and label them with the respective list entries.
    ax.set_xticks(np.arange(data.shape[1]), labels=col_labels)
    ax.set_yticks(np.arange(data.shape[0]), labels=row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False)

    ax.set_rasterized(True)

    # Rotate the tick labels and set their alignment.
    if rotatexlabels:
        plt.setp(ax.get_xticklabels(), rotation=-30, ha="right", rotation_mode="anchor")

    # Turn spines off and create white grid.
    ax.spines[:].set_visible(True)

    ax.set_xticks(np.arange(data.shape[1] + 1) - 0.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0] + 1) - 0.5, minor=True)
    ax.grid(which="minor", color="w", linewidth=3)  # , linestyle="-",)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(
    im,
    annot=None,
    data=None,
    valfmt="{x:.2f}",
    textcolors=("black", "white"),
    threshold=None,
    **textkw,
):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    annot
        List of annotations, same dimension as im
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A pair of colors.  The first is used for values below a threshold,
        the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max()) / 2.0

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center", verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            if annot is not None:
                text = im.axes.text(j, i, annot[i, j], **kw)
            else:
                text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts


def main():
    args = parseargs()
    paths = collect_experiment_filesnames(args.path, args.inclusive, args.exclusive, args.dirdepth)
    exps = [Experiment(p) for p in paths]
    category, stat = args.category, args.stat
    max_stats = {}
    # normalize
    max_stats = {
        k: max((exp.getStat(category, stat) for exp in v))
        for k, v in itertools.groupby(
            sorted(exps, key=lambda e: e.filename), key=lambda e: e.filename
        )
    }

    sorting_key = lambda e: f"{e.la}_{e.ta}"
    longest = -1
    for k, v in itertools.groupby(sorted(exps, key=sorting_key), key=sorting_key):
        longest = max(longest, len(list(v)))
    assert longest == 153

    # sort by comb
    combs = {}
    for exp in exps:
        key = sorting_key(exp)
        combs[key] = combs.get(key, []) + [exp]

    result = []
    for comb, comb_exps in combs.items():
        missing = longest - len(comb_exps)
        penalty = missing  # score_missing == 1
        # stats = [exp.getStat(category, stat) for exp in comb_exps]
        stats = [
            exp.getStat(category, stat) / max_stats.get(exp.filename, 1.0) for exp in comb_exps
        ]
        # avg_stat = np.mean(stats)
        s2_score = sum(stats) + penalty
        result.append((comb, s2_score, comb_exps))

    plot_average_stat_of_combination_heatmap(
        exps,
        category,
        stat,
        result,
        args.outdir,
        tex=True,
        prefix="".join(args.cl) if args.cl else "",
    )
    plot_average_stat_of_combination_heatmap(
        exps,
        category,
        stat,
        result,
        args.outdir,
        tex=False,
        prefix="".join(args.cl) if args.cl else "",
    )


if __name__ == "__main__":
    main()
