"""Remove all empty lines from .dot files in sub-folders"""
import os
import sys
from pathlib import Path


def find_all_files_with_suffix(path, suffix):
    """Returns a set of relative paths of files ending with suffix"""
    files = set()
    for r, d, f in os.walk(path):
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def main():
    args = sys.argv
    if len(args) != 2:
        print(f"Usage: python clean_dot.py FOLDER_WITH_DOT_FILES")
        exit(-1)

    path = Path(args[1])
    if not path.exists():
        print(f"Path {str(path)} is invalid or does not exist")
        exit(-1)

    files = find_all_files_with_suffix(path, ".dot")

    for file in files:
        print(f"Overwriting {str(file)}")
        with open(file, "r") as f:
            lines = [line for line in f if line.rstrip()]
        with open(file, "w") as f:
            f.writelines(lines)


if __name__ == "__main__":
    main()
