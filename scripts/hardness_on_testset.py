# %%
import matplotlib.pyplot as plt
import pandas as pd

try:
    from tikzplotlib import save as tikz_save
except ImportError:
    tikz_save = None
    print("WARN: tikzplotlib is not installed, no tikz plots will be created")

# %%
iname = "Automata Name"
isize = "Size Automata"
istep = "Mean ALL Steps"
ihard = "Hardness Score"

result = pd.read_csv("result.csv")

# %%

corr_method = "spearman"  # pearson, spearman, kendall
print(f"Used correlation method: {corr_method}")
corr_matrix = result[[iname, isize, istep, ihard]].corr(method=corr_method)
print(corr_matrix)

# %%

small = result[result[isize] <= 15]
large = result[result[isize] > 15]

plt.clf()
plt.xscale("log")
plt.yscale("log")
plt.scatter(small["Hardness Score"], small["Mean ALL Steps"], color="blue", label="small")
plt.scatter(large["Hardness Score"], large["Mean ALL Steps"], color="red", label="large")
plt.xlabel("test steps")
plt.ylabel("hardness score")
plt.legend()
if tikz_save is not None:
    tikz_save("log_log_plot_hardness_testset.tex")
plt.show()
