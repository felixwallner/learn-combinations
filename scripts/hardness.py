from math import log1p as log

try:
    from aalpy.base import DeterministicAutomaton
except ImportError:

    class DeterministicAutomaton:
        pass


def calculate_learn_hardness_from_params(n: int, k: int, longest_cex: int) -> float:
    """Calculates the learn hardness, which is the TTT worst case symbol complexity,
    for the following parameters of an automata:
    n: number of states of automata with n > 1 (n == 1 has no useful hardness score)
    k: size of input alphabet
    longest_cex: approximation for the length of the longest counterexample"""
    learn_hardness = (k * n**2 + n * log(longest_cex)) * (n + longest_cex)
    return learn_hardness


def calculate_test_chance_from_params(k: int, prefix_max: int, w_max: int) -> float:
    """Calculates the test chance, which is the chance of sampling the longest distinguishing sequence
    after taking the longest prefix randomely, for the following parameters of an automata:
    k: size of input alphabet
    prefix_max: length of the longest prefix
    w_max: length of the longest distinguishing sequence (of W-set)."""
    test_chance: float = 1.0 / (prefix_max * (k**w_max))
    return test_chance


def calculate_hardness_score_from_params(n: int, k: int, prefix_max: int, w_max: int) -> float:
    """Calculates the hardness score for the following parameters of an automata:
    n: number of states of automata with n > 1 (n == 1 has no useful hardness score)
    k: size of input alphabet
    prefix_max: length of the longest prefix
    w_max: length of the longest distinguishing sequence (of W-set)."""
    assert n > 0 and k > 0 and prefix_max > 0 and w_max > 0

    # m is the approximation for the longest CEX
    # as in longest prefix + longest distinguishing sequence from W
    m = prefix_max + w_max
    learn_hardness = calculate_learn_hardness_from_params(n, k, m)
    test_chance = calculate_test_chance_from_params(k, prefix_max, w_max)
    # final hardness score
    hardness_score = learn_hardness / test_chance
    return hardness_score


def calculate_hardness_score(automata: DeterministicAutomaton) -> float:
    """Calculates the hardness score for an automata.
    Score is learn_hardness / test_sampling_chance.
    The Automata should be minimal"""
    n = len(automata.states)
    if n == 1:
        # single state automata W sets are empty, no useful hardness calculation
        return 0.0

    W = automata.compute_characterization_set(online_suffix_closure=False)
    if not W:
        raise RuntimeError("W-set was empty, the automata is probabliy not minimal")
    W_lens = [len(seq) for seq in W]
    k = len(automata.get_input_alphabet())

    for s in automata.states:
        if s.prefix is None:
            s.prefix = automata.get_shortest_path(automata.initial_state, s)

    prefix_lens = [len(s.prefix) for s in automata.states]

    return calculate_hardness_score_from_params(n, k, max(prefix_lens), max(W_lens))


def _find_all_files_with_suffix(path, suffix, depth=None):
    """Returns a set of relative paths of files ending with suffix.
    If depth is an int, only go <depth> directly levels deeper"""
    import os

    files = set()
    for r, d, f in os.walk(path):
        if depth is not None and r.count(os.sep) > depth:
            print(f"INFO: Ignoring files in depth {r.count(os.sep)}:", r)
            continue
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def main(input_folder="input/mealy/principle", output_file="automata_hardness.txt"):
    from pathlib import Path

    from aalpy.utils import load_automaton_from_file
    from tqdm import tqdm

    paths = _find_all_files_with_suffix(input_folder, ".dot")
    print(f"{len(paths)} .dot files found")

    hardnessfile = Path(output_file)
    if not hardnessfile.exists():
        with hardnessfile.open("w") as f:
            for path in tqdm(sorted(map(Path, paths), key=lambda p: p.name.lower())):
                automata = load_automaton_from_file(path, "mealy")
                score = calculate_hardness_score(automata)
                print(f"{path.name}, {score}, {len(automata.states)}", file=f, flush=True)
    else:
        print(
            f"WARN: '{hardnessfile}' exists. Delete it first if you want to re-compute hardness scores."
        )

    results = []
    with hardnessfile.open("r") as f:
        for line in f:
            name, score, n = line.rsplit(", ", 2)
            name, score, n = name, float(score), int(n)
            results.append((name, score, n))

    for name, score, n in sorted(results, key=lambda x: x[1], reverse=True):
        print(f"{name[:26]:26}: {score:15.2} (n: {n})")
    print("=" * 20)
    print(f"For a total of {len(results)} automata")


if __name__ == "__main__":
    exit(main())
