# %%
import itertools as it
import os
import re
from pathlib import Path
from typing import List

import numpy as np
import pandas as pd
import tqdm
from aalpy.automata import MealyMachine
from aalpy.utils import load_automaton_from_file
from hardness import (
    calculate_hardness_score_from_params,
    calculate_learn_hardness_from_params,
    calculate_test_chance_from_params,
)

iname = "Automata Name"  # index of automata name column

# %%


def exit_err(msg):
    print(f"ERR: {msg}")
    exit(-1)


def learn_config_from_file_name(file_name: str):
    m = re.search("([A-Za-z0-9 _.\\-]+)_([A-Z]+)_([A-Z]+)\\.csv", file_name)
    if m is None:
        return None, None, None
    return m.group(1), m.group(2), m.group(3)


def read_learn_file(file_name: str):
    name, la, ta = learn_config_from_file_name(file_name)
    if any(thing is None for thing in (name, la, ta)):
        print(f"ERR: Could not learn config from filename: {file_name}")
        return None

    df = pd.read_csv(file_name, header=None, index_col=0)
    df.index.name = None
    df = df.T.reset_index(drop=True).T
    # df.columns = df.columns.droplevel(1)
    return df, name, la, ta


def find_all_files_with_suffix(path, suffix):
    """Returns a set of relative paths of files ending with suffix"""
    files = set()
    for r, d, f in os.walk(path):
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def collect_experiment_filesnames(path, inclusive, exclusive):
    """Return a list of all paths of csv files in path that include 'inclusive' and exclude 'exclusive'.
    Exit the program if this results in no files."""
    suffix = "csv"
    experiments = find_all_files_with_suffix(path, "." + suffix)
    if not experiments:
        exit_err(f"No {suffix} files were found in location {path}")

    if inclusive:
        experiments = [exp for exp in experiments if all(i in exp for i in inclusive)]
        if not experiments:
            exit_err(f"No {suffix} files remain after filtering for {inclusive}")

    if exclusive:
        experiments = [exp for exp in experiments if all(e not in exp for e in exclusive)]
        if not experiments:
            exit_err(f"No {suffix} files remain after excluding {exclusive}")

    print(f"INFO: Collected {len(experiments)} files for further analysis")
    return experiments


# %%


def fetch_and_calculate_all_results(
    folder, inclusive: List[str] | None = None, exclusive: List[str] | None = None
):
    files = collect_experiment_filesnames(folder, inclusive, exclusive)
    df = pd.DataFrame(
        columns=[
            iname,
            "Learning Algorithm",
            "Testing Algorithm",
            "Max Depth/Tests",
            "Mean ALL Steps",
            "1st-quart ALL Steps",
            "Median ALL Steps",
            "3st-quart ALL Steps",
            "Std ALL Steps",
            "Max ALL Steps",
            "Min ALL Steps",
            "Len ALL Steps",
        ]
    )
    for file in sorted(files):
        dff, name, la, ta = read_learn_file(file)
        correct_m = dff.loc["Correct M"].to_numpy(dtype=np.bool8)
        assert all(m == True for m in correct_m)
        maxdepth = dff.loc["Max Tests"].to_numpy(dtype=np.int64)
        assert all(d == maxdepth[0] for d in maxdepth)
        data = dff.loc["ALL Steps"].to_numpy(dtype=np.int64)
        df.loc[len(df)] = [
            name,
            la,
            ta,
            maxdepth[0],
            np.mean(data),
            np.percentile(data, 25),
            np.percentile(data, 50),
            np.percentile(data, 75),
            np.std(data),
            np.max(data),
            np.min(data),
            len(data),
        ]
    if len(df[iname]) != len(df[iname].unique()):
        print(
            f"WARN: {len(df[iname])} != {len(df[iname].unique())}: Results table has duplicate automata names: {set(df[iname]).difference(df[iname].unique())}. This will cause problems during merging"
        )
    return df


# %%


def fetch_and_calculate_all_automata(folder):
    def diameter(automata: MealyMachine) -> int:
        return max(
            len(automata.get_shortest_path(s1, s2))
            for s1, s2 in it.permutations(automata.states, 2)
        )

    df = pd.DataFrame(
        columns=[
            "Automata Name",
            "Size Automata",
            "Size Alphabet",
            "Size Outputs",
            "Diameter",
            "Size W Suffix Closed",
            "Size W",
            "Total Length All Ws",
            "Mean Len Ws",
            "1st-quart Len Ws",
            "Median Len Ws",
            "3rd-quart Len Ws",
            "Std Len Ws",
            "Max Len Ws",
            "Min Len Ws",
            "Mean Len Prefixes",
            "1st-quart Len Prefixes",
            "Median Len Prefixes",
            "3rd-quart Len Prefixes",
            "Std Len Prefixes",
            "Max Len Prefixes",
            "Min Len Prefixes",
            "Longest Distinguishing Sequence",
            "Learn Hardness",
            "Test Chance",
            "Hardness Score",
        ]
    )

    files = find_all_files_with_suffix(folder, "dot")
    pbar = tqdm.tqdm(sorted(files))
    for file in pbar:
        p = Path(file)
        automata_name = p.stem
        # print(f"INFO: Working on {automata_name}")
        pbar.set_description(f"Working on {automata_name}")
        if automata_name.startswith("tcp") and automata_name.endswith("_trans"):
            automata_name = automata_name[:-6]
            print(f"WARN: Interpreting {p.stem} -> {automata_name}")

        automata: MealyMachine = load_automaton_from_file(p, "mealy", True)
        if not automata.is_input_complete():
            print(f"INFO: Automata '{automata_name}' is not input complete")

        n = len(automata.states)
        W = automata.compute_characterization_set(online_suffix_closure=False)
        if W == []:
            if n > 1:
                print(f"WARN: Automata '{automata_name}' is not minimal i.e. the W-set is empty.")
            else:
                print(f"WARN: Ignoring one-state automata '{automata_name}'")
            continue
        k = len(automata.get_input_alphabet())
        dia = diameter(automata)
        Ws = [len(seq) for seq in W]
        prefixes = [len(s.prefix) for s in automata.states]
        # m is the approximation for the longest CEX
        # as in longest prefix + longest distinguishing sequence from W
        m = np.max(prefixes) + np.max(Ws)
        Wsuf = automata.compute_characterization_set(online_suffix_closure=True)
        learn_hardness = calculate_learn_hardness_from_params(n, k, m)
        test_chance = calculate_test_chance_from_params(k, np.max(prefixes), np.max(Ws))
        hardness_score = calculate_hardness_score_from_params(n, k, np.max(prefixes), np.max(Ws))
        df.loc[len(df)] = [
            automata_name,
            n,  # number of states
            k,  # size of input alphabet
            len([output for state in automata.states for output in state.output_fun.values()]),
            dia,
            len(Wsuf),
            len(W),
            sum(Ws),
            np.mean(Ws),
            np.percentile(Ws, 25),
            np.percentile(Ws, 50),
            np.percentile(Ws, 75),
            np.std(Ws),
            np.max(Ws),
            np.min(Ws),
            # prefix lens
            np.mean(prefixes),
            np.percentile(prefixes, 25),
            np.percentile(prefixes, 50),
            np.percentile(prefixes, 75),
            np.std(prefixes),
            np.max(prefixes),
            np.min(prefixes),
            m,  # longest CEX
            learn_hardness,
            test_chance,
            hardness_score,
        ]
    if len(df[iname]) != len(df[iname].unique()):
        print(
            f"WARN: Automata info table has duplicate automata names: {set(df[iname]).difference(df[iname].unique())}. This will cause problems during merging"
        )
    return df


# %%


def fetch_and_calculate_combined():
    use_only_combination = "TTT_RANDOMWPMETHOD"
    # use_only_combination = "LSTARRS_RANDOMWPMETHOD"
    print(f"INFO: Using *only* the combination {use_only_combination} for evaluation")

    # df_res = fetch_and_calculate_all_results("learn_results", inclusive=["LSTARRS_RANDOMWPMETHOD"])
    df_res = fetch_and_calculate_all_results("results", inclusive=[use_only_combination])
    df_aut = fetch_and_calculate_all_automata("input/mealy/principle")
    single_automata = set(df_res[iname]).symmetric_difference(df_aut[iname])
    if single_automata:
        print(
            f"WARN: Same automata are only in one of the tables: {single_automata}. The resulting OUTER merge will leave cells incomplete while the INNER merge will drop them."
        )
    df = pd.merge(df_res, df_aut, how="outer", on=iname)
    df_inner = pd.merge(df_res, df_aut, how="inner", on=iname)
    return df, df_inner


def main():
    df_outer, df_inner = fetch_and_calculate_combined()
    df_outer.to_csv("result-all.csv", index=False)
    df_inner.to_csv("result.csv", index=False)


if __name__ == "__main__":
    main()
