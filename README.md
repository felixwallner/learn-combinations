# Learn-Combinations

## About

This project is the implementation of a testing framework for Mealy machines to evaluate different combinations of learning and testing algorithms for active automata benchmark systems.
It was developed as part of the Bachelor thesis [Benchmarking Active Automata Learning Configurations]() by Felix Wallner at Graz University of Technology.

The work of this thesis was later published in [Benchmarking Combinations of Learning and Testing Algorithms for Active Automata Learning](https://link.springer.com/chapter/10.1007/978-3-030-50995-8_1) at [TAP 2020: Tests and Proofs](https://link.springer.com/book/10.1007/978-3-030-50995-8).

The latest version of the code and results in this repository also includes ADT and active RPNI for an extended - at the moment unpublished - version of the conference paper above for a journal.

## Build

Make sure to have Maven and Java 10 installed!

Recommended versions are:

* `Apache Maven 3.6.0`
* `OpenJDK 10.0.2`

First run:

```
mvn initialize
```

This will install the local JAR versions of [Mut-Learn](https://github.com/mtappler/mut-learn) into the repository as Mut-Learn is not available via Maven Central.

Then run:
```
mvn compile
```
This will compile the project into the `./target/` folder.

## Run

After having run *Build* use the following command to execute the program:

```
mvn exec:java
```

This will take the input files from `./input/mealy/principle/*` and save the results into `./out/*`. This behaviour can be changed inside the `Main.java` class.

Which learning and testing algorithms should be used during the benchmark can be changed in the `src/main/java/utils/Eval.java` class. By default all combinations aside from RPNI are used.

*Note*: The entire runtime of all input files might takes several days, especially because the TCP examples are very big and take a long time. To run only a fraction of the input files, remove all unwanted files from the folder(s) `./input/*` or set the input path in `Main.java`, recompile and then run.

## Evaluation

Use the file `stats.py` to evaluate the CSV output files.

Required:
* Python 3.8+
* Numpy 1.23+
* Matplotlib 3.6+

Run `python3 stats.py` if you want to evaluate your own generated results from the folder `./out/*`.

Run `python3 stats.py -p results` if you want to evaluate the finished results from the folder `./results/*`.

Run `python3 stats.py -h` to see all the options available.

## More Data and Scripts

A number of additional scripts for closer analysis of the produced data are in the `./scripts/` folder. These scripts are not as polished as `stats.py` and may require code changes instead of being parametrised. 
For most analysis `stats.py` should be used instead. 
These scripts require additional requirements in `./scripts/requirements.txt`. (They might also require newer versions of libraries and Python 3.10+)
All scripts should be called from the root directory like so `python3 scripts/name_of_script.py`

* `aggregate.py`: Produces the `result.csv` table of one combination, including steps, size and hardness of all automata. (the default and checked in version of this file uses the **TTT** + **random Wp-method** combination)
* `classify.py`: Produces the `classification.txt` file. (Most of the classification information can also be taken from `result.csv`)
* `clean_dot.py`: Removes empty lines from `.dot` files. Mostly a utility for easier parsing.
* `hardness.py`: Contains the calculation for the hardness-score which may be imported or copied from this file. If called directly will calculate the hardness-scores of all `.dot` files in path. (This information may also be taken from `result.csv`)
* `hardness_on_testset.py`: Produces a plot of the hardness and steps from `result.csv`.
* `stats_build_heatmap.py`: Produces a heat map from the results from `./out/*` or use with `-p results` to generate it from all results. (Uses the normalised-sum score with penalty for missing experiments by default)
